package storage

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"

	"gitlab.com/fokurly/tamada-app/events/service"
)

func (s *Storage) CreateEvent(ctx context.Context, in service.CreateEventIn) error {
	queryInsert := `
		INSERT INTO event_info (
		        user_creator_id,
				name,             
				start_time,      
				end_time,       
				address,       
				address_link,      
				address_additional,
				is_expenses,  
				important,   
				theme,   
				dress_code,  
				moodboad_link, 
				cover_id          
		)
		values (
		        :user_creator_id,
				:name,
		        :startTime,
		        :endTime,
		        :address,
		        :addressLink,
		        :addressAdditional,
		        :IsExpenses,
		        :important,
		        :theme,
		        :dressCode,
		        :moodBoardLink,
		        :coverID
		) RETURNING id
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"user_creator_id":   in.UserCreatorID,
			"name":              in.Name,
			"startTime":         in.StartTime,
			"endTime":           in.EndTime,
			"address":           in.Address,
			"addressLink":       in.AddressLink,
			"addressAdditional": in.AddressAdditional,
			"IsExpenses":        in.IsExpenses,
			"important":         in.Important,
			"theme":             in.Theme,
			"dressCode":         in.DressCode,
			"moodBoardLink":     in.MoodboadLink,
			"coverID":           in.CoverID,
		}
		var partyId int64
		// подумать че сделать чтобы еще контекст туда пихать
		rows, err := tx.NamedQuery(queryInsert, payload)
		if err != nil {
			panic("kfmewfkmwef")
		}
		//defer rows.Close()
		if rows.Next() {
			err := rows.Scan(&partyId)
			if err != nil {
				tx.Rollback()
				return err
			}
		}

		queryInsert = `
		INSERT INTO party_admins (
			      party_id, 
		          creator_id
		)
		values (
		        :party_id,
		        :creator_id
		)
		`
		payload = map[string]any{
			"party_id":   partyId,
			"creator_id": in.UserCreatorID,
		}
		_, ierr := tx.NamedExecContext(ctx, queryInsert, payload)
		if ierr != nil {
			return fmt.Errorf("can't insert event admins. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) CreateEventAnotherTransaction(ctx context.Context, in service.CreateEventIn) error {
	insertEvent := `
		INSERT INTO event_info (
		        user_creator_id,
				name,             
				start_time,      
				end_time,       
				address,       
				address_link,      
				address_additional,
				is_expenses,  
				important,   
				theme,   
				dress_code,  
				moodboad_link, 
				cover_id          
		)
		values (
		        $1,
				$2,
		        $3,
		        $4,
		        $5,
		        $6,
		        $7,
		        $8,
		        $9,
		        $10,
		        $11,
		        $12,
		        $13
		) RETURNING id
	`

	txn, err := s.dbx.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		// Rollback the transaction after the function returns.
		// If the transaction was already commited, this will do nothing.
		_ = txn.Rollback()
	}()

	var partyId int64
	err = txn.QueryRowContext(ctx, insertEvent, in.UserCreatorID,
		in.Name,
		in.StartTime,
		in.EndTime,
		in.Address,
		in.AddressLink,
		in.AddressAdditional,
		in.IsExpenses,
		in.Important,
		in.Theme,
		in.DressCode,
		in.MoodboadLink,
		in.CoverID,
	).Scan(&partyId)
	if err != nil {
		return fmt.Errorf("could not insert event. txx error: %w", err)
	}

	insertAdmins := `
		INSERT INTO party_admins (
			      party_id, 
		          creator_id
		)
		values (
		        :party_id,
		        :creator_id
		)
		`
	payload := map[string]any{
		"party_id":   partyId,
		"creator_id": in.UserCreatorID,
	}
	_, ierr := txn.NamedExecContext(ctx, insertAdmins, payload)
	if ierr != nil {
		return fmt.Errorf("can't insert event admins. error: %w", ierr)
	}

	// позже перенести в отдельный модуль
	insertPartyToUser := `
		INSERT INTO user_parties (
		          user_id,
			      party_id, 
		          role
		)
		values (
		        :user_id,
		        :party_id,
		        :role
		)
		`
	payload = map[string]any{
		"user_id":  in.UserCreatorID,
		"party_id": partyId,
		"role":     service.CreatorRole,
	}
	_, ierr = txn.NamedExecContext(ctx, insertPartyToUser, payload)
	if ierr != nil {
		return fmt.Errorf("can't insert event admins. error: %w", ierr)
	}

	queryInsert := `
		INSERT INTO wallet (
			      party_id
		)
		values (
		        :party_id
		)
		`
	payload = map[string]any{
		"party_id": partyId,
	}
	_, ierr = txn.NamedExecContext(ctx, queryInsert, payload)
	if ierr != nil {
		return fmt.Errorf("can't insert new wallet. error: %w", ierr)
	}
	// Commit the transaction.
	return txn.Commit()
}

func (s *Storage) GetEvent(ctx context.Context, partyID int64) (*service.PartyFullInfo, error) {
	queryGet := `
		SELECT id,
		       user_creator_id,
		       name,
		       start_time,
		       end_time,
		       address,
		       address_link,
		       address_additional,
		       is_expenses,
		       important,
		       theme,
		       dress_code,
		       moodboad_link,
		       cover_id,
			   created_at
		FROM event_info WHERE id=:party_id
	`

	payload := map[string]any{
		"party_id": partyID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var eventsInfo []service.PartyFullInfo
	if rows.Next() {
		var eventInfo service.PartyFullInfo
		err := rows.StructScan(&eventInfo)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}
		eventsInfo = append(eventsInfo, eventInfo)
	}

	if len(eventsInfo) == 0 {
		return nil, fmt.Errorf("could not find party with id: %d", partyID)
	}
	return &eventsInfo[0], nil
}

func (s *Storage) GetUserEvents(ctx context.Context, userID int64) ([]service.UserParty, error) {
	queryGet := `
		SELECT user_id, party_id, role
		FROM user_parties WHERE user_id=:userID
	`

	payload := map[string]any{
		"userID": userID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var userParties []service.UserParty
	for rows.Next() {
		var userParty service.UserParty
		err := rows.StructScan(&userParty)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}
		userParties = append(userParties, userParty)
	}

	return userParties, nil
}

func (s *Storage) GetPartiesGuests(ctx context.Context, partyID int64) ([]service.PartiesGuests, error) {
	queryGet := `
		SELECT user_id, role, status
		FROM user_parties WHERE party_id=:partyID
	`

	payload := map[string]any{
		"partyID": partyID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var partiesGuests []service.PartiesGuests
	for rows.Next() {
		var partiesGuest service.PartiesGuests
		err := rows.StructScan(&partiesGuest)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}
		partiesGuests = append(partiesGuests, partiesGuest)
	}

	return partiesGuests, nil
}

func (s *Storage) DeleteUserFromParty(ctx context.Context, deleteFlow service.DeleteUserFromPartyIn) (bool, error) {
	queryGet := `
		SELECT user_id, role
		FROM user_parties WHERE party_id=:partyID and user_id=:userID
	`

	queryDelete := `
		DELETE FROM user_parties
		 WHERE party_id=:partyID and user_id=:userID
	`

	payload := map[string]any{
		"partyID": deleteFlow.PartyID,
		"userID":  deleteFlow.ActionFrom,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return false, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	if rows.Next() {
		var userInfo service.UserRole
		err := rows.StructScan(&userInfo)
		if err != nil {
			return false, fmt.Errorf("could not scan rows. error: %v", err)
		}
		if userInfo.Role == service.ParticipantRole {
			return false, fmt.Errorf("forbidden action. user %d is participant", deleteFlow.ActionFrom)
		} else if userInfo.Role == service.CreatorRole {
			err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
				payload := map[string]any{
					"partyID": deleteFlow.PartyID,
					"userID":  deleteFlow.UserID,
				}

				if _, ierr := tx.NamedExecContext(ctx, queryDelete, payload); ierr != nil {
					return fmt.Errorf("can't delete user. error: %w", ierr)
				}

				return nil
			})
			if err != nil {
				return false, fmt.Errorf("could not delete user. transaction error: %w", err)
			}
		} else {
			payload := map[string]any{
				"partyID": deleteFlow.PartyID,
				"userID":  deleteFlow.UserID,
			}

			rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
			if err != nil {
				return false, fmt.Errorf("can't get rows: %w", err)
			}
			defer func() {
				_ = rows.Close()
			}()

			type UserInf struct {
				UserID int64  `json:"user_id"`
				Role   string `json:"role"`
			}

			if rows.Next() {
				var userInfo UserInf
				err := rows.StructScan(&userInfo)
				if err != nil {
					return false, fmt.Errorf("could not scan rows. error: %v", err)
				}
				if userInfo.Role != service.CreatorRole {
					err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
						payload := map[string]any{
							"partyID": deleteFlow.PartyID,
							"userID":  deleteFlow.UserID,
						}

						if _, ierr := tx.NamedExecContext(ctx, queryDelete, payload); ierr != nil {
							return fmt.Errorf("can't delete user. error: %w", ierr)
						}

						return nil
					})
					if err != nil {
						return false, fmt.Errorf("could not delete user. transaction error: %w", err)
					}
				} else {
					return false, fmt.Errorf("forbidden to delete creator")
				}
			}
		}

	} else {
		return false, fmt.Errorf("userID %d not found in party %d", deleteFlow.ActionFrom, deleteFlow.PartyID)
	}

	return true, nil
}

func (s *Storage) AddUserToParty(ctx context.Context, addFlow service.AddUserToParty) error {
	queryInsertUser := `
		insert into user_parties (user_id, party_id, role) 
		values (
		        :user_id,
				:party_id,
		        :role
		)
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"user_id":  addFlow.UserID,
			"party_id": addFlow.PartyID,
			"role":     service.ParticipantRole,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsertUser, payload); ierr != nil {
			return fmt.Errorf("can't add user. error: %w", ierr)
		}
		return nil
	})

	if err != nil {
		return fmt.Errorf("transaction failed. err: %w", err)
	}
	return nil
}

func (s *Storage) UpdateUserRoleOnParty(ctx context.Context, changeRoleFlow service.UpdateUserRoleOnPartyIn) error {
	actionFromRole, err := s.GetUserRole(ctx, changeRoleFlow.ActionFrom, changeRoleFlow.PartyID)
	if err != nil || actionFromRole == nil {
		return fmt.Errorf("could not get role for user: %d. err: %w", changeRoleFlow.ActionFrom, err)
	}

	userID, err := s.GetUserRole(ctx, changeRoleFlow.UserID, changeRoleFlow.PartyID)
	if err != nil {
		return fmt.Errorf("could not get role for user: %d", actionFromRole.UserID)
	}

	if userID == nil {
		return fmt.Errorf("invalid user or party id")
	}

	if actionFromRole.Role == service.CreatorRole || (actionFromRole.Role == service.ManagerRole && userID.Role != service.CreatorRole) {
		queryUpdate := `
		UPDATE user_parties SET role=:newRole
		WHERE party_id=:partyID and user_id=:userID
	`
		err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
			payload := map[string]any{
				"userID":  changeRoleFlow.UserID,
				"partyID": changeRoleFlow.PartyID,
				"newRole": changeRoleFlow.NewRole,
			}
			if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
				return fmt.Errorf("can't add user. error: %w", ierr)
			}
			return nil
		})
		if err != nil {
			return fmt.Errorf("transaction failed. err: %w", err)
		}
	} else {
		return fmt.Errorf("forbidden action. action from role %s and userID role %s", actionFromRole.Role, userID.Role)
	}

	return nil
}

func (s *Storage) GetUserRole(ctx context.Context, userID, partyID int64) (*service.UserRole, error) {
	queryGet := `
		SELECT user_id, role
		FROM user_parties WHERE party_id=:partyID and user_id=:userID
	`

	payload := map[string]any{
		"partyID": partyID,
		"userID":  userID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	if rows.Next() {
		var userInfo service.UserRole
		err := rows.StructScan(&userInfo)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return &userInfo, nil
	}

	return nil, nil
}

// перед update проверяется роль юзера
func (s *Storage) UpdateEventStartTime(ctx context.Context, startTime string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET start_time=:startTime
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"startTime": startTime,
			"partyID":   partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventName(ctx context.Context, name string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET name=:name
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"name":    name,
			"partyID": partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventEndTime(ctx context.Context, endTime string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET end_time=:endTime
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"endTime": endTime,
			"partyID": partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventAddress(ctx context.Context, address string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET address=:address
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"address": address,
			"partyID": partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventAddressLink(ctx context.Context, addressLink string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET address_link=:addressLink
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"addressLink": addressLink,
			"partyID":     partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventAddressAdditionalInfo(ctx context.Context, addressAdditional string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET address_additional=:addressAdditional
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"addressAdditional": addressAdditional,
			"partyID":           partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventIsExpenses(ctx context.Context, isExpenses int64, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET is_expenses=:isExpenses
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"isExpenses": isExpenses,
			"partyID":    partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventImportant(ctx context.Context, important string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET important=:important
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"important": important,
			"partyID":   partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventTheme(ctx context.Context, theme string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET theme=:theme
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"theme":   theme,
			"partyID": partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventDressCode(ctx context.Context, dressCode string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET dress_code=:dressCode
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"dressCode": dressCode,
			"partyID":   partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventMoodboardLink(ctx context.Context, moodboard string, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET moodboad_link=:moodboard
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"moodboard": moodboard,
			"partyID":   partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateEventCoverID(ctx context.Context, coverID int64, partyID int64) error {
	queryUpdate := `
		UPDATE event_info SET cover_id=:coverID
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"coverID": coverID,
			"partyID": partyID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) AddListToEvent(ctx context.Context, in service.AddListToEventIn) (int64, error) {
	queryInsert := `
		INSERT INTO list (party_id, type) values (
		        $1,
				$2
		) RETURNING id
	`
	txn, err := s.dbx.BeginTxx(ctx, nil)
	if err != nil {
		return 0, err
	}
	defer func() {
		// Rollback the transaction after the function returns.
		// If the transaction was already commited, this will do nothing.
		_ = txn.Rollback()
	}()

	var partyId int64
	err = txn.QueryRowContext(ctx, queryInsert, in.PartyID, in.Type).Scan(&partyId)
	if err != nil {
		return 0, fmt.Errorf("could not list to event. txx error: %w", err)
	}

	return partyId, txn.Commit()
}

func (s *Storage) UpdateListVisibility(ctx context.Context, in service.UpdateListVisibilityIn) error {
	queryUpdate := `
		UPDATE list SET managers_only=:isVisible
		WHERE id=:listID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"isVisible": in.IsVisible,
			"listID":    in.ListID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update event info. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) AddTasksToList(ctx context.Context, in service.AddTasksToListIn) error {
	queryInsertUser := `insert into task (name, list_id) values (   
                :name,
				:listID)`

	txn, err := s.dbx.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		// Rollback the transaction after the function returns.
		// If the transaction was already commited, this will do nothing.
		_ = txn.Rollback()
	}()

	for i := range in.Tasks {
		payload := map[string]any{
			"name":   in.Tasks[i].Name,
			"listID": in.ListID,
		}
		_, ierr := txn.NamedExecContext(ctx, queryInsertUser, payload)
		if ierr != nil {
			return fmt.Errorf("can't insert event admins. error: %w", ierr)
		}
	}

	return txn.Commit()
}

func (s *Storage) UpdateTaskStatus(ctx context.Context, in service.UpdateTaskStatusIn) error {
	queryUpdate := `
		UPDATE task SET done=:isDone
		WHERE id=:taskID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"isDone": in.IsDone,
			"taskID": in.TaskID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update task status. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateTaskName(ctx context.Context, in service.UpdateTaskNameIn) error {
	queryUpdate := `
		UPDATE task SET name=:newName
		WHERE id=:taskID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"newName": in.NewName,
			"taskID":  in.TaskID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update task name. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateListType(ctx context.Context, listID int64, newType string) error {
	queryUpdate := `
		UPDATE list SET type=:newType
		WHERE id=:listID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"newType": newType,
			"listID":  listID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update task name. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdateFinanceState(ctx context.Context, partyID int64, financeState int64) error {
	queryUpdate := `
		UPDATE event_info SET is_expenses=:financeState
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID":      partyID,
			"financeState": financeState,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update finance state. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) GetPartyLists(ctx context.Context, partyID int64) ([]service.ListInfo, error) {
	queryGet := `
		SELECT id, party_id, type, managers_only
		FROM list WHERE party_id=:partyID
	`

	payload := map[string]any{
		"partyID": partyID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var lists []service.ListInfo
	for rows.Next() {
		var list service.ListInfo
		err := rows.StructScan(&list)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		lists = append(lists, list)
	}

	return lists, nil
}

func (s *Storage) GetListInfoForCurrentParty(ctx context.Context, listID int64, partyID int64) (*service.ListInfo, error) {
	queryGet := `
		SELECT id, party_id, type, managers_only
		FROM list WHERE party_id=:partyID and id=:listID
	`

	payload := map[string]any{
		"partyID": partyID,
		"listID":  listID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	//var userParties []service.UserParty
	if rows.Next() {
		var list service.ListInfo
		err := rows.StructScan(&list)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return &list, nil
	}

	return nil, nil
}

func (s *Storage) GetTasksInfo(ctx context.Context, listID int64) ([]service.TaskInfo, error) {
	queryGet := `
		SELECT id, name, done
		FROM task WHERE list_id=:listID
	`

	payload := map[string]any{
		"listID": listID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var tasks []service.TaskInfo
	for rows.Next() {
		var task service.TaskInfo
		err := rows.StructScan(&task)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (s *Storage) DeleteList(ctx context.Context, partyID int64, listID int64) error {
	queryDelete := `
		DELETE FROM list
		 WHERE party_id=:partyID and id=:listID
	`

	payload := map[string]any{
		"partyID": partyID,
		"listID":  listID,
	}

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		if _, ierr := tx.NamedExecContext(ctx, queryDelete, payload); ierr != nil {
			return fmt.Errorf("can't delete list. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) InsertNewUserReceipt(ctx context.Context, userID, partyID int64, name string, sum float64, typee string) (int64, error) {
	insertExpense := `
		INSERT INTO expense (
		        party_id,
		        user_id,
		        name,
		        sum,
		        type
		)
		values (
		        $1,
				$2,
		        $3,
		        $4,
		        $5
		) RETURNING id
	`

	txn, err := s.dbx.BeginTxx(ctx, nil)
	if err != nil {
		return 0, err
	}
	defer func() {
		// Rollback the transaction after the function returns.
		// If the transaction was already commited, this will do nothing.
		_ = txn.Rollback()
	}()

	var expenseID int64
	err = txn.QueryRowContext(ctx, insertExpense, partyID, userID, name, sum, typee).Scan(&expenseID)
	if err != nil {
		return 0, fmt.Errorf("could not insert expense. txx error: %w", err)
	}

	err = txn.Commit()
	if err != nil {
		return 0, fmt.Errorf("could not commit. txx error: %w", err)
	}
	return expenseID, nil
}

func (s *Storage) GetAllUserExpenses(ctx context.Context, userID, partyID int64) ([]service.UserExpenses, error) {
	queryGet := `
		SELECT id, party_id, user_id, name, sum, type
		FROM expense WHERE user_id=:userID AND party_id=:partyID
	`

	payload := map[string]any{
		"partyID": partyID,
		"userID":  userID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var userExpenses []service.UserExpenses
	for rows.Next() {
		var val service.UserExpenses
		err := rows.StructScan(&val)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		userExpenses = append(userExpenses, val)
	}

	return userExpenses, nil
}

func (s *Storage) GetUserExpenseByID(ctx context.Context, userID, partyID, expenseID int64) (*service.UserExpenses, error) {
	queryGet := `
		SELECT id, party_id, user_id, name, sum, type
		FROM expense WHERE id=:expenseID and user_id=:userID
	`

	payload := map[string]any{
		"expenseID": expenseID,
		"userID":    userID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	if rows.Next() {
		var val service.UserExpenses
		err := rows.StructScan(&val)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return &val, nil
	}

	return nil, nil
}

func (s *Storage) GetAllPartyExpenses(ctx context.Context, partyID int64) ([]service.UserExpenses, error) {
	queryGet := `
		SELECT id, party_id, user_id, name, sum, type
		FROM expense WHERE type=:typee AND party_id=:partyID
	`

	payload := map[string]any{
		"partyID": partyID,
		"typee":   "SUM",
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var userExpenses []service.UserExpenses
	for rows.Next() {
		var val service.UserExpenses
		err := rows.StructScan(&val)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		userExpenses = append(userExpenses, val)
	}

	return userExpenses, nil
}

func (s *Storage) GetPartyWallet(ctx context.Context, partyID int64) (*service.GetPartyWalletOut, error) {
	queryGet := `
		SELECT id, party_id, card_number, card_owner, phone_number, bank
		FROM wallet where party_id=:partyID
	`

	payload := map[string]any{
		"partyID": partyID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	if rows.Next() {
		var val service.GetPartyWalletOut
		err := rows.StructScan(&val)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return &val, nil
	}

	return nil, nil
}

func (s *Storage) UpdateWalletPhone(ctx context.Context, partyID int64, phone string) error {
	queryUpdate := `
		UPDATE wallet SET phone_number=:phone
		WHERE id=:partyID
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID": partyID,
			"phone":   phone,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryUpdate, payload); ierr != nil {
			return fmt.Errorf("can't update finance state. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}

func (s *Storage) UpdatePartyWalletCard(ctx context.Context, userID int64, cardNumber int64) error {
	queryInsert := `
		UPDATE wallet SET card_number=:cardNumber WHERE party_id=:partyID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID":    userID,
			"cardNumber": cardNumber,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update card number. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdatePartyWalletOwner(ctx context.Context, userID int64, cardOwner string) error {
	queryInsert := `
		UPDATE wallet SET card_owner=:cardOwner WHERE party_id=:partyID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID":   userID,
			"cardOwner": cardOwner,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update card owner. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdatePartyWalletPhone(ctx context.Context, userID int64, phone string) error {
	queryInsert := `
		UPDATE wallet SET phone_number=:phone WHERE party_id=:partyID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID": userID,
			"phone":   phone,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update phone. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdatePartyWalletBank(ctx context.Context, userID int64, bank string) error {
	queryInsert := `
		UPDATE wallet SET bank=:bank WHERE party_id=:partyID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID": userID,
			"bank":    bank,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update bank. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateReceiptName(ctx context.Context, userID int64, partyID int64, expenseID int64, name string) error {
	queryInsert := `
		UPDATE expense SET name=:name WHERE user_id=:userID and id=:expenseID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID":    userID,
			"expenseID": expenseID,
			"name":      name,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update name of reciept. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateReceiptSum(ctx context.Context, userID int64, partyID int64, expenseID int64, sum float64) error {
	queryInsert := `
		UPDATE expense SET sum=:sum WHERE user_id=:userID and id=:expenseID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID":    userID,
			"expenseID": expenseID,
			"sum":       sum,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update sum of reciept. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateExpensesDeadline(ctx context.Context, partyID int64, deadline string) error {
	queryInsert := `
		UPDATE event_info SET deadline_expenses=:deadline WHERE id=:partyID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"partyID":  partyID,
			"deadline": deadline,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update expenses deadline of reciept. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) GetPartyExpensesDeadline(ctx context.Context, partyID int64) (*service.GetPartyExpensesDeadlineOut, error) {
	queryGet := `
		SELECT deadline_expenses
		FROM event_info WHERE id=:party_id
	`

	payload := map[string]any{
		"party_id": partyID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	if rows.Next() {
		var eventInfo service.GetPartyExpensesDeadlineOut
		err := rows.StructScan(&eventInfo)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return &eventInfo, nil
	}

	return nil, nil
}

func (s *Storage) DeleteTaskFromList(ctx context.Context, taskID, listID int64) error {
	queryDelete := `
		DELETE FROM task
		 WHERE id=:taskID and list_id=:listID
	`

	payload := map[string]any{
		"taskID": taskID,
		"listID": listID,
	}

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		if _, ierr := tx.NamedExecContext(ctx, queryDelete, payload); ierr != nil {
			return fmt.Errorf("can't delete task. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction error: %w", err)
	}

	return nil
}
