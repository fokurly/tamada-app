package storage

import "github.com/jmoiron/sqlx"

type Storage struct {
	dbx *sqlx.DB
}

func NewStorage(dbx *sqlx.DB) *Storage {
	return &Storage{
		dbx: dbx,
	}
}

type Txerx interface {
	Beginx() (*sqlx.Tx, error)
}
