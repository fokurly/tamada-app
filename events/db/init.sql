CREATE TABLE IF NOT EXISTS event_info
(
    id                 bigserial unique,
    user_creator_id    integer,
    name               varchar(100) NOT NULL,
    start_time         varchar(100),
    end_time           varchar(100),
    address            varchar(500),
    address_link       varchar(1000),
    address_additional varchar(1000),
    is_expenses        integer,
    deadline_expenses  varchar(1000)         default '',
    important          varchar(3000),
    theme              varchar(3000),
    dress_code         varchar(3000),
    moodboad_link      varchar(3000),
    cover_id           integer,
    created_at         timestamp    not null default now()
);

CREATE TABLE IF NOT EXISTS party_admins
(
    party_id   integer unique,
    creator_id integer,
    moderators bigint[],
    foreign key (party_id) references event_info (id)
);

CREATE TYPE user_role AS ENUM ('manager', 'creator', 'participant');

CREATE TYPE status AS ENUM ('confirmed', 'uncertain', 'unconfirmed');

CREATE TYPE typeList AS ENUM ('EMPTY', 'TODO', 'BUY', 'WISHLIST');

-- dept - чек, при отправке денег другому человеку, sum - при покупке расходников на вечеринку
CREATE TYPE expenseType AS ENUM ('DEBT', 'SUM');

CREATE TABLE IF NOT EXISTS user_parties
(
    user_id  bigint,
    party_id integer,
    role     user_role,
    status   status default 'confirmed',
    primary key (party_id, user_id),
    foreign key (party_id) references event_info (id)
);

-- managers_only = isVisible -> true - видно всем, false - только менеджерам
CREATE TABLE IF NOT EXISTS list
(
    id            bigserial unique,
    party_id      integer,
    type          typeList default 'EMPTY',
    managers_only bool     default false,
    foreign key (party_id) references event_info (id)
);

CREATE TABLE IF NOT EXISTS task
(
    id      bigserial unique,
    name    varchar(4000),
    done    bool default false,
    list_id bigserial,
    foreign key (list_id) references list (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS expense
(
    id       bigserial unique,
    party_id integer,
    user_id  bigint,
    name     varchar(4000),
    sum      decimal(19, 4),
    type     expenseType,
    primary key (user_id, name),
    foreign key (party_id) references event_info (id)
);

CREATE TABLE IF NOT EXISTS wallet
(
    id           bigserial unique,
    party_id     integer,
    card_number  bigint       default 0,
    card_owner   varchar(200) default '',
    phone_number varchar(16)  default '',
    bank         varchar(100) default '',
    foreign key (party_id) references event_info (id)
);