package dto

type GetUserPartiesOut struct {
	PartyID   int64  `json:"partyID"`
	PartyName string `json:"name"`
	CoverID   int64  `json:"coverID"`
	IsManager bool   `json:"isManager"`
	IsNew     bool   `json:"isNew"` // 3 дня после создания
}
