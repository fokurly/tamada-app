package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"
	"github.com/unidoc/unipdf/v3/common/license"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"github.com/jmoiron/sqlx"

	"gitlab.com/fokurly/tamada-app/events/service"
	"gitlab.com/fokurly/tamada-app/events/storage"
)

func main() {
	mainCtx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	db, err := initDB()
	if err != nil {
		logrus.Panicf(fmt.Sprintf("could not open db. err: %v", err))
	}
	eventStorage := storage.NewStorage(db)

	eventService := service.NewEventService(eventStorage)
	router := gin.Default()
	router.POST("/create_event", eventService.CreateEvent)
	router.POST("/get_event", eventService.GetEvent)

	// перенести все связанное с юзером в другое место
	router.POST("/get_user_parties", eventService.GetUserParties)
	router.POST("/get_parties_guests", eventService.GetPartiesGuests)
	router.POST("/delete_user_from_party", eventService.DeleteUserFromParty)
	router.POST("/update_user_role_on_party", eventService.UpdateUserRoleOnParty)
	router.POST("/add_user_to_party", eventService.AddUserToParty)

	router.POST("/update_event_cover_id", eventService.UpdateEventCoverID)
	router.POST("/update_event_moodboard_link", eventService.UpdateEventMoodboardLink)
	router.POST("/update_event_dress_code", eventService.UpdateEventDressCode)
	router.POST("/update_event_theme", eventService.UpdateEventTheme)
	router.POST("/update_event_important", eventService.UpdateEventImportant)
	router.POST("/update_event_is_expenses", eventService.UpdateEventIsExpenses)
	router.POST("/update_event_address_additional", eventService.UpdateEventAddressAdditionalInfo)
	router.POST("/update_event_address_link", eventService.UpdateEventAddressLink)
	router.POST("/update_event_address", eventService.UpdateEventAddress)
	router.POST("/update_event_end_time", eventService.UpdateEventEndTime)
	router.POST("/update_event_start_time", eventService.UpdateEventStartTime)
	router.POST("/update_event_name", eventService.UpdateEventName)

	router.POST("/add_list_to_event", eventService.AddListToEvent)
	router.POST("/is_list_visible_to_guest", eventService.UpdateListVisibility)
	router.POST("/add_task_to_list", eventService.AddTaskToList)
	router.POST("/update_task_status", eventService.UpdateTaskStatus)
	router.POST("/update_task_name", eventService.UpdateTaskName)
	router.POST("/get_list_info", eventService.GetListInfo)

	router.POST("/get_party_lists", eventService.GetPartyLists)
	router.POST("/delete_list", eventService.DeleteList)
	router.POST("/update_list_type", eventService.UpdateListType)

	router.POST("/get_invite_link", eventService.GetInviteLinkToEvent)
	router.POST("/load_finance_state", eventService.LoadFinanceState)
	router.POST("/update_finance_state", eventService.UpdateFinanceState)
	router.POST("/update_receipt", eventService.UploadReceipt)
	router.POST("/update_receipt_info_sum", eventService.UploadReceiptInfoSum)
	router.POST("/update_receipt_info_name", eventService.UploadReceiptInfoName)

	router.POST("/update_party_expenses_deadline", eventService.UpdatePartyExpensesDeadline)
	router.POST("/get_all_user_receipts", eventService.GetAllUserReceipt)
	router.POST("/get_user_receipt", eventService.GetUserReceipt)
	router.POST("/get_user_expenses_by_id", eventService.GetUserExpenseInfoByID)

	router.POST("/get_party_summary_expenses", eventService.GetPartySummaryExpenses)
	router.POST("/get_party_wallet", eventService.GetPartyWallet)

	router.POST("/update_party_wallet_owner", eventService.UpdatePartyWalletOwner)
	router.POST("/update_party_wallet_bank", eventService.UpdatePartyWalletBank)
	router.POST("/update_party_wallet_phone", eventService.UpdatePartyWalletPhone)
	router.POST("/update_party_wallet_card", eventService.UpdatePartyWalletCard)
	router.POST("/calculate_expenses", eventService.CalculateExpenses)
	router.POST("/get_party_expenses_deadline", eventService.GetPartyExpensesDeadline)
	router.POST("/delete_task_from_list", eventService.DeleteTaskFromList)

	server := http.Server{
		Addr:    ":8082",
		Handler: router,
	}
	g, gCtx := errgroup.WithContext(mainCtx)
	g.Go(func() error {
		return server.ListenAndServe()
	})
	g.Go(func() error {
		<-gCtx.Done()
		return server.Shutdown(context.Background())
	})

	if err := g.Wait(); err != nil {
		logrus.Print("exit reason: %s \n", err)
	}
}

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		PrettyPrint:     true,
	})

	err := license.SetMeteredKey("d0979a2d9f82cc56c5f9681964ec5405de3cdbc6a52a6df02b0bf12e62de864d")
	if err != nil {
		panic("err license")
	}
}

func initDB() (*sqlx.DB, error) {
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	dbName := os.Getenv("POSTGRES_DB")
	if pass == "" {
		user = "postgres"
		pass = "pass"
		dbName = "postgres"
		// для локалки
		query := fmt.Sprintf("host=0.0.0.0 port=5432 user=%s password=%s dbname=%s sslmode=disable", user, pass, dbName)
		return sqlx.Open("postgres", query)
	}
	// для локалки
	query := fmt.Sprintf("host=db user=%s password=%s dbname=%s sslmode=disable", user, pass, dbName)
	return sqlx.Open("postgres", query)
}
