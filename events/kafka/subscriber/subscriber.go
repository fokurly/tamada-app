package subscriber

import (
	"context"
	"errors"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"

	"gitlab.com/fokurly/tamada-app/events/service"
)

func (k *KafkaConsumer) ReadTopic(ctx context.Context) {
	logrus.Info("Start read topic")
	for {
		select {
		case <-ctx.Done():
			return
		default:
			msg, err := k.consumer.ReadMessage(100)
			if err != nil {
				var kafkaErr kafka.Error
				ok := errors.As(err, &kafkaErr)
				if !ok {
					logrus.Warnf("failed to cast err to kafka err: %v", err)
					time.Sleep(time.Duration(1) * time.Second)
					continue
				}
				switch kafkaErr.Code() {
				case kafka.ErrTimedOut:
					logrus.Tracef("read message timeout, err: %s", kafkaErr.Error())
				case kafka.ErrPartitionEOF:
					logrus.Tracef("no more messages, err: %s", kafkaErr.Error())
				default:
					logrus.Warnf("failed to read message, err: %s", kafkaErr.Error())
				}
				time.Sleep(time.Duration(1) * time.Second)
				continue
			}

			var event UserToDelete
			err = jsoniter.Unmarshal(msg.Value, &event)
			if err != nil {
				logrus.Errorf("could not unmarshal msg from topic %s. body: %s", msg.TopicPartition.Topic, msg.Value)
				continue
			}
			logrus.Info("event type", "delete")

			_, err = k.repo.DeleteUserFromParty(ctx, service.DeleteUserFromPartyIn{
				UserID: event.UserID,
			})
			if err != nil {
				logrus.Errorf("could not insert trip info. error: %s", err.Error())
				continue
			}
			_, err = k.consumer.CommitMessage(msg)

		}
	}
}

type UserToDelete struct {
	UserID int64 `json:"userID"`
}
