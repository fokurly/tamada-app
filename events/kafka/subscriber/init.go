package subscriber

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/sirupsen/logrus"

	"gitlab.com/fokurly/tamada-app/events/storage"
)

type KafkaConsumer struct {
	consumer *kafka.Consumer
	repo     storage.Storage
}

func NewKafkaConsumer(repo storage.Storage) *KafkaConsumer {
	consumer, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":  "localhost:9094",
		"group.id":           "default",
		"auto.offset.reset":  "earliest",
		"enable.auto.commit": true,
	})

	if err != nil {
		logrus.Panicf("could not init kafka consumer.err: %v", err)
	}
	err = consumer.SubscribeTopics([]string{"delete"}, nil)
	if err != nil {
		logrus.Panicf("could not init kafka consumer. error: %v", err)
	}
	logrus.Info("Consumer up!")
	return &KafkaConsumer{
		consumer: consumer,
	}
}
