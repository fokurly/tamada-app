package service

import (
	"context"
)

type EventService struct {
	storage Storage
}

func NewEventService(storage Storage) *EventService {
	return &EventService{
		storage: storage,
	}
}

type Storage interface {
	CreateEvent(ctx context.Context, in CreateEventIn) error
	CreateEventAnotherTransaction(ctx context.Context, in CreateEventIn) error
	GetEvent(ctx context.Context, partyID int64) (*PartyFullInfo, error)
	GetUserEvents(ctx context.Context, userID int64) ([]UserParty, error)
	GetPartiesGuests(ctx context.Context, partyID int64) ([]PartiesGuests, error)
	DeleteUserFromParty(ctx context.Context, deleteFlow DeleteUserFromPartyIn) (bool, error)
	AddUserToParty(ctx context.Context, addFlow AddUserToParty) error
	UpdateUserRoleOnParty(ctx context.Context, changeRole UpdateUserRoleOnPartyIn) error
	UpdateEventCoverID(ctx context.Context, coverID int64, partyID int64) error
	UpdateEventMoodboardLink(ctx context.Context, moodboard string, partyID int64) error
	UpdateEventDressCode(ctx context.Context, dressCode string, partyID int64) error
	UpdateEventTheme(ctx context.Context, theme string, partyID int64) error
	UpdateEventImportant(ctx context.Context, important string, partyID int64) error
	UpdateEventIsExpenses(ctx context.Context, isExpenses int64, partyID int64) error
	UpdateEventAddressAdditionalInfo(ctx context.Context, addressAdditional string, partyID int64) error
	UpdateEventAddressLink(ctx context.Context, addressLink string, partyID int64) error
	UpdateEventAddress(ctx context.Context, address string, partyID int64) error
	UpdateEventEndTime(ctx context.Context, endTime string, partyID int64) error
	UpdateEventStartTime(ctx context.Context, startTime string, partyID int64) error
	GetUserRole(ctx context.Context, userID, partyID int64) (*UserRole, error)
	AddListToEvent(ctx context.Context, in AddListToEventIn) (int64, error)
	UpdateListVisibility(ctx context.Context, in UpdateListVisibilityIn) error
	AddTasksToList(ctx context.Context, in AddTasksToListIn) error
	UpdateTaskStatus(ctx context.Context, in UpdateTaskStatusIn) error
	UpdateTaskName(ctx context.Context, in UpdateTaskNameIn) error
	GetListInfoForCurrentParty(ctx context.Context, listID int64, partyID int64) (*ListInfo, error)
	GetTasksInfo(ctx context.Context, listID int64) ([]TaskInfo, error)
	GetPartyLists(ctx context.Context, partyID int64) ([]ListInfo, error)
	DeleteList(ctx context.Context, partyID int64, listID int64) error
	UpdateListType(ctx context.Context, listID int64, newType string) error
	UpdateFinanceState(ctx context.Context, partyID int64, financeState int64) error
	InsertNewUserReceipt(ctx context.Context, userID, partyID int64, name string, sum float64, typee string) (int64, error)
	GetAllUserExpenses(ctx context.Context, userID, partyID int64) ([]UserExpenses, error)
	GetUserExpenseByID(ctx context.Context, userID, partyID, expenseID int64) (*UserExpenses, error)
	GetAllPartyExpenses(ctx context.Context, partyID int64) ([]UserExpenses, error)
	UpdateEventName(ctx context.Context, name string, partyID int64) error
	GetPartyWallet(ctx context.Context, partyID int64) (*GetPartyWalletOut, error)

	UpdatePartyWalletCard(ctx context.Context, userID int64, cardNumber int64) error
	UpdatePartyWalletOwner(ctx context.Context, userID int64, cardOwner string) error
	UpdatePartyWalletPhone(ctx context.Context, userID int64, phone string) error
	UpdatePartyWalletBank(ctx context.Context, userID int64, bank string) error

	UpdateReceiptName(ctx context.Context, userID int64, partyID int64, expenseID int64, name string) error
	UpdateReceiptSum(ctx context.Context, userID int64, partyID int64, expenseID int64, sum float64) error
	UpdateExpensesDeadline(ctx context.Context, partyID int64, deadline string) error
	GetPartyExpensesDeadline(ctx context.Context, partyID int64) (*GetPartyExpensesDeadlineOut, error)
	DeleteTaskFromList(ctx context.Context, taskID, listID int64) error
}
