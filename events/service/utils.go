package service

import "fmt"

func BuildInviteLink(partyID int64) string {
	return fmt.Sprintf("http://tamada/%d", partyID)
}
