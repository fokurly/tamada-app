package service

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/unidoc/unipdf/v3/common/license"

	"gitlab.com/fokurly/tamada-app/events/dto"
)

// SignIn получаем access + refresh token
func (a *EventService) CreateEvent(ctx *gin.Context) {
	var event CreateEventIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&event); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}
	//rand.Seed(time.Now().UnixNano())
	rand.New(rand.NewSource(time.Now().Unix()))
	event.CoverID = rand.Int63() % 4
	err := a.storage.CreateEventAnotherTransaction(ctx, event)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not create event. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) GetEvent(ctx *gin.Context) {
	var param GetPartyIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	event, err := a.storage.GetEvent(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get event. error: %v", err))
		return
	}

	ctx.JSON(http.StatusOK, event)
}

func (a *EventService) GetUserParties(ctx *gin.Context) {
	var param GetUserPartiesIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	userEvents, err := a.storage.GetUserEvents(ctx, param.UserID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get event. error: %v", err))
		return
	}

	if len(userEvents) == 0 {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}
	var eventsInfo []PartyFullInfo
	for i := range userEvents {
		eventInfo, err := a.storage.GetEvent(ctx, userEvents[i].PartyID)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get event. error: %v", err))
			return
		}

		eventsInfo = append(eventsInfo, *eventInfo)
	}

	castedResult := CastUserEventsOut(userEvents, eventsInfo)

	ctx.JSON(http.StatusOK, castedResult)
}

func (a *EventService) GetPartiesGuests(ctx *gin.Context) {
	var param GetPartiesGuestsIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	partiesGuests, err := a.storage.GetPartiesGuests(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get event. error: %v", err))
		return
	}

	if len(partiesGuests) == 0 {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ctx.JSON(http.StatusOK, partiesGuests)
}

const threeDays = 72

func CastUserEventsOut(userParties []UserParty, partiesInfo []PartyFullInfo) []dto.GetUserPartiesOut {
	partyMap := make(map[int64]UserParty)
	for i := range userParties {
		partyMap[userParties[i].PartyID] = userParties[i]
	}

	result := make([]dto.GetUserPartiesOut, 0, len(partiesInfo))
	for i := range partiesInfo {
		if val, ok := partyMap[partiesInfo[i].PartyID]; ok {
			isManager := false
			if val.Role != ParticipantRole {
				isManager = true
			}

			timee, err := time.Parse(time.RFC3339, *partiesInfo[i].CreatedDate)
			if err != nil {
				panic(err)
			}

			isNew := false
			if time.Now().Sub(timee).Hours() < threeDays {
				isNew = true
			}

			result = append(result, dto.GetUserPartiesOut{
				PartyID:   partiesInfo[i].PartyID,
				PartyName: partiesInfo[i].Name,
				CoverID:   partiesInfo[i].CoverID,
				IsManager: isManager,
				IsNew:     isNew,
			})
		}
	}

	return result
}

func (a *EventService) DeleteUserFromParty(ctx *gin.Context) {
	var param DeleteUserFromPartyIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	_, err := a.storage.DeleteUserFromParty(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not delete user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateUserRoleOnParty(ctx *gin.Context) {
	var param UpdateUserRoleOnPartyIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateUserRoleOnParty(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) AddUserToParty(ctx *gin.Context) {
	var param AddUserToParty
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.AddUserToParty(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventStartTime(ctx *gin.Context) {
	var param UpdateEventStartTimeIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventStartTime(ctx, param.NewStartTime, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventName(ctx *gin.Context) {
	var param UpdateEventNameIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventName(ctx, param.Name, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventEndTime(ctx *gin.Context) {
	var param UpdateEventEndTimeIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventEndTime(ctx, param.NewEndTime, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventAddress(ctx *gin.Context) {
	var param UpdateEventAddressIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventAddress(ctx, param.Address, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventAddressLink(ctx *gin.Context) {
	var param UpdateEventAddressLinkIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventAddressLink(ctx, param.AddressLink, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventAddressAdditionalInfo(ctx *gin.Context) {
	var param UpdateEventAddressAdditionalInfoIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventAddressAdditionalInfo(ctx, param.AddressAdditionalInfo, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventIsExpenses(ctx *gin.Context) {
	var param UpdateEventIsExpensesIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventIsExpenses(ctx, param.IsExpenses, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventImportant(ctx *gin.Context) {
	var param UpdateEventImportantIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventImportant(ctx, param.Important, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventTheme(ctx *gin.Context) {
	var param UpdateEventThemeIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventTheme(ctx, param.Theme, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventDressCode(ctx *gin.Context) {
	var param UpdateEventDressCodeIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventDressCode(ctx, param.DressCode, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventMoodboardLink(ctx *gin.Context) {
	var param UpdateEventMoodboardLinkIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventMoodboardLink(ctx, param.MoodboardLink, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateEventCoverID(ctx *gin.Context) {
	var param UpdateEventCoverIDIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetUserRole(ctx, param.ActionFrom, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not check user role. err: %v", err))
		return
	}
	if info == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find user: %d", param.ActionFrom))
		return
	}
	if info.Role == ParticipantRole {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("not allowed to change event info"))
		return
	}

	err = a.storage.UpdateEventCoverID(ctx, param.CoverID, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not add user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) AddListToEvent(ctx *gin.Context) {
	var param AddListToEventIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	id, err := a.storage.AddListToEvent(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not create list. error: %v", err))
		return
	}
	res := AddListToEventOut{
		ListID: id,
	}

	ctx.JSON(http.StatusOK, res)
}

func (a *EventService) UpdateListVisibility(ctx *gin.Context) {
	var param UpdateListVisibilityIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateListVisibility(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not create list. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) AddTaskToList(ctx *gin.Context) {
	var param AddTasksToListIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.AddTasksToList(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not create list. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateTaskStatus(ctx *gin.Context) {
	var param UpdateTaskStatusIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateTaskStatus(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update task. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateTaskName(ctx *gin.Context) {
	var param UpdateTaskNameIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateTaskName(ctx, param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update task. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) GetListInfo(ctx *gin.Context) {
	var param GetListInfoIn
	// добавить required/omitempty
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	list, err := a.storage.GetListInfoForCurrentParty(ctx, param.ListID, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get list. error: %v", err))
		return
	}

	if list == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not find list"))
		return
	}

	tasks, err := a.storage.GetTasksInfo(ctx, param.ListID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get tasks. error: %v", err))
		return
	}
	var res GetListInfoOut
	res.ListID = param.ListID
	res.Type = list.Type
	res.IsVisible = list.IsVisible
	res.PartyID = list.PartyID
	if len(tasks) == 0 {
		res.Tasks = []TaskInfo{}
	}
	res.Tasks = tasks
	ctx.JSON(http.StatusOK, res)
}

func (a *EventService) GetPartyLists(ctx *gin.Context) {
	var param GetPartyListsIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	lists, err := a.storage.GetPartyLists(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get lists. error: %v", err))
		return
	}

	ctx.JSON(http.StatusOK, lists)
}

func (a *EventService) DeleteList(ctx *gin.Context) {
	var param DeleteListIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.DeleteList(ctx, param.PartyID, param.ListID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not delete lists. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdateListType(ctx *gin.Context) {
	var param UpdateListTypeIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateListType(ctx, param.ListID, param.NewType)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not delete lists. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) GetInviteLinkToEvent(ctx *gin.Context) {
	var param GetInviteLinkIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	_, err := a.storage.GetEvent(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not get party with id %d. error: %v", param.PartyID, err))
		return
	}

	link := BuildInviteLink(param.PartyID)
	ctx.JSON(http.StatusOK, GetInviteLinkOut{
		Link: link,
	})
}

func (a *EventService) LoadFinanceState(ctx *gin.Context) {
	var param LoadFinanceStateIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	eventInfo, err := a.storage.GetEvent(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not get party with id %d. error: %v", param.PartyID, err))
		return
	}

	ctx.JSON(http.StatusOK, LoadFinanceStateOut{
		IsExpenses: eventInfo.IsExpenses,
	})
}

func (a *EventService) UpdateFinanceState(ctx *gin.Context) {
	var param UpdateFinanceStateIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateFinanceState(ctx, param.PartyID, param.FinanceState)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not get party with id %d. error: %v", param.PartyID, err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UploadReceipt(ctx *gin.Context) {
	var param UploadReceiptIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	expenseID, err := a.storage.InsertNewUserReceipt(ctx, param.UserID, param.PartyID, param.Name, param.Sum, param.Type)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("error saving expenses: %v", err))
		return
	}

	ctx.JSON(http.StatusOK, UploadReceiptOut{
		ExpensesID: expenseID,
	})
}

func (a *EventService) UploadReceiptInfoSum(ctx *gin.Context) {
	var param UploadReceiptInfoSumIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateReceiptSum(ctx, param.UserID, param.PartyID, param.ExpenseID, param.NewSum)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("error saving expenses: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UploadReceiptInfoName(ctx *gin.Context) {
	var param UploadReceiptInfoNameIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateReceiptName(ctx, param.UserID, param.PartyID, param.ExpenseID, param.NewName)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("error saving expenses: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdatePartyExpensesDeadline(ctx *gin.Context) {
	var param UpdatePartyExpensesDeadlineIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateExpensesDeadline(ctx, param.PartyID, param.ExpensesDeadline)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("error saving expenses: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) GetAllUserReceipt(ctx *gin.Context) {
	var param GetAllUserReceiptIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := a.storage.GetAllUserExpenses(ctx, param.UserID, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not get user expenses. error: %v", err))
		return
	}
	if len(info) == 0 {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ctx.JSON(http.StatusOK, info)
}

func (a *EventService) GetUserExpenseInfoByID(ctx *gin.Context) {
	var params GetUserExpenseIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	val, err := a.storage.GetUserExpenseByID(ctx, params.UserID, params.PartyID, params.ExpenseID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user expenses. error: %v", err))
		return
	}

	if val == nil {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ctx.JSON(http.StatusOK, val)
}

func (a *EventService) GetUserReceipt(ctx *gin.Context) {
	var param GetUserReceiptIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	ctx.File(fmt.Sprintf("receipt/partyID_%d/userID_%d/%d.pdf", param.PartyID, param.UserID, param.ExpenseID))
}

// только sum, а не debt
func (a *EventService) GetPartySummaryExpenses(ctx *gin.Context) {
	var param GetPartySummaryExpensesIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	expenses, err := a.storage.GetAllPartyExpenses(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	var res float64
	for i := range expenses {
		res += expenses[i].Sum
	}

	ctx.JSON(http.StatusOK, GetPartySummaryExpensesOut{
		TotalSum: res,
	})
}

func (a *EventService) GetPartyWallet(ctx *gin.Context) {
	var param GetPartyWalletIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	wallet, err := a.storage.GetPartyWallet(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	if wallet == nil {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ctx.JSON(http.StatusOK, wallet)
}

func (a *EventService) UpdatePartyWalletOwner(ctx *gin.Context) {
	var param UpdatePartyWalletOwnerIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdatePartyWalletOwner(ctx, param.PartyID, param.Owner)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdatePartyWalletBank(ctx *gin.Context) {
	var param UpdatePartyWalletBankIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdatePartyWalletBank(ctx, param.PartyID, param.Bank)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdatePartyWalletPhone(ctx *gin.Context) {
	var param UpdatePartyWalletPhoneIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdatePartyWalletPhone(ctx, param.PartyID, param.PhoneNumber)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) UpdatePartyWalletCard(ctx *gin.Context) {
	var param UpdatePartyWalletCardIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdatePartyWalletCard(ctx, param.PartyID, param.Card)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) GetPartyExpensesDeadline(ctx *gin.Context) {
	var param GetPartyExpensesDeadlineIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	res, err := a.storage.GetPartyExpensesDeadline(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	if res == nil {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ctx.JSON(http.StatusOK, res)
}

func (a *EventService) DeleteTaskFromList(ctx *gin.Context) {
	var param DeleteTaskFromListIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.DeleteTaskFromList(ctx, param.TaskID, param.ListID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not delete task. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *EventService) CalculateExpenses(ctx *gin.Context) {
	var param CalculateExpensesIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	expenses, err := a.storage.GetAllPartyExpenses(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party expenses. error: %v", err))
		return
	}

	// общая сумма вечеринки
	var totalPartySum float64
	for i := range expenses {
		totalPartySum += expenses[i].Sum
	}

	guests, err := a.storage.GetPartiesGuests(ctx, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get party guests. error: %v", err))
		return
	}

	if len(guests) == 0 {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("no guests on party"))
		return
	}
	// количество гостей на мероприятии
	numGuests := len(guests)

	firstDebt := totalPartySum / float64(numGuests)
	firstRes := fmt.Sprintf("1) %f ÷ %d = %f (стоимость с человека)", totalPartySum, numGuests, firstDebt)

	userExpenses, err := a.storage.GetAllUserExpenses(ctx, param.UserID, param.PartyID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user expenses. error: %v", err))
		return
	}

	var totalUserExpenses float64
	for i := range userExpenses {
		totalUserExpenses += userExpenses[i].Sum
	}

	userDebt := firstDebt - totalUserExpenses
	secondRes := ""
	if userDebt >= 0 {
		secondRes = fmt.Sprintf("2) %f - %f = %f (вы должны)", firstDebt, totalUserExpenses, userDebt)
	}
	secondRes = fmt.Sprintf("2) %f - %f = %f -> (вам должны %f)", firstDebt, totalUserExpenses, userDebt, -userDebt)

	ctx.JSON(http.StatusOK, CalculateExpensesOut{
		ExpenseForUser: firstRes,
		UserDebtString: secondRes,
		UserDebt:       userDebt,
	})
}
