package service

type PartyFullInfo struct {
	UserCreatorID     int64   `json:"userCreatorID" db:"user_creator_id"`
	PartyID           int64   `json:"partyID" db:"id"`
	Name              string  `json:"name" db:"name"`
	StartTime         string  `json:"startTime" db:"start_time"`
	EndTime           string  `json:"endTime" db:"end_time"`
	Address           string  `json:"address" db:"address"`
	AddressLink       string  `json:"addressLink" db:"address_link"`
	AddressAdditional string  `json:"addressAdditional" db:"address_additional"`
	IsExpenses        int64   `json:"isExpenses" db:"is_expenses"` // 0-3 4 разных состояния, клиент засылает
	Important         string  `json:"important" db:"important"`
	Theme             string  `json:"theme" db:"theme"`
	DressCode         string  `json:"dressCode" db:"dress_code"`
	MoodboadLink      string  `json:"moodboadLink" db:"moodboad_link"`
	CoverID           int64   `json:"coverID" db:"cover_id"` // обложка, рандом изначально
	CreatedDate       *string `json:"createdDate" db:"created_at" binding:"omitempty"`
}

type CreateEventIn struct {
	UserCreatorID     int64  `json:"userCreatorID" binding:"required"`
	Name              string `json:"name"`
	StartTime         string `json:"startTime"`
	EndTime           string `json:"endTime"`
	Address           string `json:"address"`
	AddressLink       string `json:"addressLink"`
	AddressAdditional string `json:"addressAdditional"`
	IsExpenses        int64  `json:"isExpenses"` // 0-3 4 разных состояния, клиент засылает
	Important         string `json:"important"`
	Theme             string `json:"theme"`
	DressCode         string `json:"dressCode"`
	MoodboadLink      string `json:"moodboadLink"`
	CoverID           int64  `json:"coverID"` // обложка, рандом изначально
}

type GetPartyIn struct {
	PartyID int64 `json:"party_id" binding:"required"`
}

type GetUserPartiesIn struct {
	UserID int64 `json:"userID" binding:"required"`
}

type GetPartiesGuestsIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartiesGuestsOut struct {
	Login    string `json:"login"`
	UserID   int64  `json:"userID"`
	Role     string `json:"role"`
	AvatarID string `json:"avatarID"`
	Status   string `json:"status"`
}

const ManagerRole = "manager"
const CreatorRole = "creator"
const ParticipantRole = "participant"

const StatusConfirmed = "confirmed"
const StatusUncertain = "uncertain"
const StatusUnconfirmed = "unconfirmed"

type UserParty struct {
	UserID  int64  `db:"user_id"`
	PartyID int64  `db:"party_id"`
	Role    string `db:"role"`
}

type PartiesGuests struct {
	UserID int64 `json:"userID" db:"user_id"`
	Role   string
	Status string
}

type UserRole struct {
	UserID int64  `db:"user_id"`
	Role   string `db:"role"`
}

type DeleteUserFromPartyIn struct {
	UserID     int64 `json:"userToDelete"` // юзер которого удаляют
	ActionFrom int64 `json:"actionFrom"`   // id юзера, который удаляет
	PartyID    int64 `json:"partyID"`
}

type AddUserToParty struct {
	UserID  int64 `json:"userID"` // юзер которого удаляют
	PartyID int64 `json:"partyID"`
}

type UpdateUserRoleOnPartyIn struct {
	UserID     int64  `json:"userID"`
	ActionFrom int64  `json:"actionFrom"`
	PartyID    int64  `json:"partyID"`
	NewRole    string `json:"newRole"` // available confirmed, uncertain, unconfirmed
}

type UpdateEventStartTimeIn struct {
	ActionFrom   int64  `json:"actionFrom"`
	PartyID      int64  `json:"partyID"`
	NewStartTime string `json:"newStartTime"`
}

type UpdateEventEndTimeIn struct {
	ActionFrom int64  `json:"actionFrom"`
	PartyID    int64  `json:"partyID"`
	NewEndTime string `json:"newEndTime"`
}

type UpdateEventAddressIn struct {
	ActionFrom int64  `json:"actionFrom"`
	PartyID    int64  `json:"partyID"`
	Address    string `json:"address"`
}

type UpdateEventAddressLinkIn struct {
	ActionFrom  int64  `json:"actionFrom"`
	PartyID     int64  `json:"partyID"`
	AddressLink string `json:"addressLink"`
}

type UpdateEventAddressAdditionalInfoIn struct {
	ActionFrom            int64  `json:"actionFrom"`
	PartyID               int64  `json:"partyID"`
	AddressAdditionalInfo string `json:"addressAdditionalInfo"`
}

type UpdateEventIsExpensesIn struct {
	ActionFrom int64 `json:"actionFrom"`
	PartyID    int64 `json:"partyID"`
	IsExpenses int64 `json:"isExpenses"`
}

type UpdateEventImportantIn struct {
	ActionFrom int64  `json:"actionFrom"`
	PartyID    int64  `json:"partyID"`
	Important  string `json:"important"`
}

type UpdateEventThemeIn struct {
	ActionFrom int64  `json:"actionFrom"`
	PartyID    int64  `json:"partyID"`
	Theme      string `json:"theme"`
}

type UpdateEventDressCodeIn struct {
	ActionFrom int64  `json:"actionFrom"`
	PartyID    int64  `json:"partyID"`
	DressCode  string `json:"dressCode"`
}

type UpdateEventMoodboardLinkIn struct {
	ActionFrom    int64  `json:"actionFrom"`
	PartyID       int64  `json:"partyID"`
	MoodboardLink string `json:"moodboardLink"`
}

type UpdateEventCoverIDIn struct {
	ActionFrom int64 `json:"actionFrom"`
	PartyID    int64 `json:"partyID"`
	CoverID    int64 `json:"coverID"`
}

const ListEmpty = "EMPTY"
const ListTODO = "TODO"
const ListBuy = "BUY"
const ListWith = "WISHLIST"

type AddListToEventIn struct {
	PartyID int64  `json:"partyID"`
	Type    string `json:"name" binding:"required,oneof=EMPTY TODO BUY WISHLIST"`
}

type AddListToEventOut struct {
	ListID int64 `json:"listID"`
}

type DeleteListIn struct {
	PartyID int64 `json:"partyID"`
	ListID  int64 `json:"listID"`
}

type TaskCreateIn struct {
	Name string `json:"name"`
}

type AddTasksToListIn struct {
	PartyID int64          `json:"partyID"`
	ListID  int64          `json:"listID"`
	Tasks   []TaskCreateIn `json:"tasks"`
}

type UpdateListVisibilityIn struct {
	ListID    int64 `json:"listID"`
	IsVisible bool  `json:"isVisible"`
}

type UpdateTaskStatusIn struct {
	TaskID int64 `json:"taskID"`
	IsDone bool  `json:"isDone"`
}

type UpdateTaskNameIn struct {
	TaskID  int64  `json:"taskID"`
	NewName string `json:"newName"`
}

type GetListInfoIn struct {
	ListID  int64 `json:"listID"`
	PartyID int64 `json:"partyID"`
}

type GetListInfoOut struct {
	ListID    int64      `json:"listID"`
	Type      string     `json:"type"`
	PartyID   int64      `json:"partyID"`
	IsVisible string     `json:"isVisible"`
	Tasks     []TaskInfo `json:"tasks"`
}

type TaskInfo struct {
	TaskID int64  `json:"taskID" db:"id"`
	Name   string `json:"name" db:"name"`
	IsDone bool   `json:"isDone" db:"done"`
}

type ListInfo struct {
	Type      string `json:"type"`
	PartyID   int64  `json:"party_id" db:"party_id"`
	ListID    int64  `json:"id" db:"id"`
	IsVisible string `json:"isVisible" db:"managers_only"`
}

type GetPartyListsIn struct {
	PartyID int64 `json:"partyID"`
}

type UpdateListTypeIn struct {
	ListID  int64  `json:"listID" binding:"required"`
	NewType string `json:"newType" binding:"required,oneof=EMPTY TODO BUY WISHLIST"`
}

type GetInviteLinkIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetInviteLinkOut struct {
	Link string `json:"link"`
}

type LoadFinanceStateIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type LoadFinanceStateOut struct {
	IsExpenses int64 `json:"financeState" binding:"required"`
}

type UpdateFinanceStateIn struct {
	PartyID      int64 `json:"partyID" binding:"required"`
	FinanceState int64 `json:"financeState" binding:"required"`
	UserID       int64 `json:"userID" binding:"required"`
}

type GetAllUserReceiptIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
	UserID  int64 `json:"userID" binding:"required"`
}

type UploadReceiptOut struct {
	ExpensesID int64 `json:"expensesID"`
}

type UserExpenses struct {
	PartyID  int64   `json:"partyID" db:"party_id"`
	UserID   int64   `json:"userID" db:"user_id"`
	Name     string  `json:"name"  db:"name"`
	Sum      float64 `json:"sum" db:"sum"`
	Type     string  `json:"type" db:"type"`
	Expenses int64   `json:"expensesID" db:"id"`
}

type GetUserExpenseIn struct {
	UserID    int64 `json:"userID" binding:"required"`
	PartyID   int64 `json:"partyID" binding:"required"`
	ExpenseID int64 `json:"expenseID" binding:"required"`
}

type GetUserReceiptIn struct {
	UserID    int64 `json:"userID" binding:"required"`
	PartyID   int64 `json:"partyID" binding:"required"`
	ExpenseID int64 `json:"expenseID" binding:"required"`
}

type GetPartySummaryExpensesIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartySummaryExpensesOut struct {
	TotalSum float64 `json:"totalSum"`
}

type UploadReceiptIn struct {
	Name    string  `json:"name" binding:"required"`
	Sum     float64 `json:"sum" binding:"required"`
	UserID  int64   `json:"userID" binding:"required"`
	PartyID int64   `json:"partyID" binding:"required"`
	Type    string  `json:"type" binding:"required,oneof=SUM DEBT"`
}

type UploadReceiptInfoSumIn struct {
	NewSum    float64 `json:"newSum" binding:"required"`
	UserID    int64   `json:"userID" binding:"required"`
	PartyID   int64   `json:"partyID" binding:"required"`
	ExpenseID int64   `json:"expenseID" binding:"required"`
}

type UploadReceiptInfoNameIn struct {
	NewName   string `json:"newName" binding:"required"`
	UserID    int64  `json:"userID" binding:"required"`
	PartyID   int64  `json:"partyID" binding:"required"`
	ExpenseID int64  `json:"expenseID" binding:"required"`
}

type UpdatePartyExpensesDeadlineIn struct {
	PartyID          int64  `json:"partyID" binding:"required"`
	ExpensesDeadline string `json:"ExpensesDeadline"  binding:"omitempty"`
}

type UpdateEventNameIn struct {
	PartyID    int64  `json:"partyID" binding:"required"`
	Name       string `json:"name" binding:"required"`
	ActionFrom int64  `json:"actionFrom" binding:"required"`
}

type GetPartyWalletIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartyWalletOut struct {
	PartyID     int64  `json:"partyID"  db:"party_id"`
	WalletID    int64  `json:"walletID" db:"id"`
	CardOwner   string `json:"cardOwner" db:"card_owner"`
	CardNumber  string `json:"cardNumber" db:"card_number"`
	PhoneNumber string `json:"phoneNumber" db:"phone_number"`
	Bank        string `json:"bank" db:"bank"`
}

type UpdatePartyWalletOwnerIn struct {
	PartyID int64  `json:"partyID" binding:"required"`
	Owner   string `json:"owner" binding:"required"`
}

type UpdatePartyWalletBankIn struct {
	PartyID int64  `json:"partyID" binding:"required"`
	Bank    string `json:"bank" binding:"required"`
}

type UpdatePartyWalletPhoneIn struct {
	PartyID     int64  `json:"partyID" binding:"required"`
	PhoneNumber string `json:"phoneNumber" binding:"required"`
}

type UpdatePartyWalletCardIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
	Card    int64 `json:"cardNumber" binding:"required"`
}

type CalculateExpensesIn struct {
	UserID  int64 `json:"userID" binding:"required"`
	PartyID int64 `json:"partyID" binding:"required"`
}

type CalculateExpensesOut struct {
	ExpenseForUser string  `json:"expenseForUser"`
	UserDebtString string  `json:"userDebtString"`
	UserDebt       float64 `json:"userDebt"`
}

type GetPartyExpensesDeadlineIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartyExpensesDeadlineOut struct {
	DeadlineExpenses string `json:"deadlineExpenses" db:"deadline_expenses"`
}

type DeleteTaskFromListIn struct {
	ListID int64 `json:"listID" binding:"required"`
	TaskID int64 `json:"taskID" binding:"required"`
}
