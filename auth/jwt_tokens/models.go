package jwt_tokens

import (
	jwtGo "github.com/dgrijalva/jwt-go"
)

type JwtCustomClaims struct {
	ID  string `json:"id"`
	UID string `json:"uid"`
	jwtGo.StandardClaims
}

type CachedTokens struct {
	AccessUID  string `json:"access"`
	RefreshUID string `json:"refresh"`
}
