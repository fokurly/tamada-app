package jwt_tokens

import "context"

type TokenService struct {
	storage Storage
}

func NewTokenService(storage Storage) *TokenService {
	return &TokenService{
		storage: storage,
	}
}

type Storage interface {
	InsertTokens(ctx context.Context, userLogin, accessToken, refreshToken, uuid string) error
	CheckAccessToken(ctx context.Context, accessToken string) (bool, error)
	CheckRefreshToken(ctx context.Context, accessToken string) (bool, error)
	UpdateTokens(ctx context.Context, login, refreshToken, accessToken, uuid string) error
}
