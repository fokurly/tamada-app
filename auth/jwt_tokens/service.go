package jwt_tokens

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	jwtGo "github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

func (tokenService *TokenService) createToken(login string, expireMinutes int) *JwtCustomClaims {
	exp := time.Now().Add(time.Minute * time.Duration(expireMinutes)).Unix()
	uid := uuid.New().String()
	claims := &JwtCustomClaims{
		ID:  login,
		UID: uid,
		StandardClaims: jwtGo.StandardClaims{
			ExpiresAt: exp,
		},
	}

	return claims
}

const ExpireAccessMinutes = 10080
const ExpireRefreshMinutes = 20000

const JwtKey = `my_secret_key`

func (tokenService *TokenService) GetNewTokens(login string) (string, string, error) {
	accessToken := tokenService.createToken(login, ExpireAccessMinutes)
	token := jwtGo.NewWithClaims(jwtGo.SigningMethodHS256, accessToken)
	accessString, err := token.SignedString([]byte(JwtKey))
	if err != nil {
		return "", "", fmt.Errorf("could not create tokens. err: %w", err)
	}

	refreshToken := tokenService.createToken(login, ExpireRefreshMinutes)
	token = jwtGo.NewWithClaims(jwtGo.SigningMethodHS256, refreshToken)
	refreshString, err := token.SignedString([]byte(JwtKey))
	if err != nil {
		return "", "", fmt.Errorf("could not create tokens. err: %w", err)
	}

	return accessString, refreshString, nil
}

func (tokenService *TokenService) CheckToken(token string) (bool, error) {
	claims := &JwtCustomClaims{}
	tkn, err := jwtGo.ParseWithClaims(token, claims, func(token *jwtGo.Token) (interface{}, error) {
		return []byte(JwtKey), nil
	})

	if err != nil {
		if errors.Is(err, jwtGo.ErrSignatureInvalid) {
			// сделать обертку на ошибку в handler
			return false, fmt.Errorf("invalid in token")
		}

		strError := err.Error()
		if strings.Contains(strError, "expired") {
			return false, nil
		}
		return false, fmt.Errorf("could not check token. err: %w", err)
	}
	if !tkn.Valid {
		return false, nil
	}

	return true, nil
}

func (tokenService *TokenService) RefreshToken(token string) (string, string, error) {
	claims := &JwtCustomClaims{}
	_, err := jwtGo.ParseWithClaims(token, claims, func(token *jwtGo.Token) (interface{}, error) {
		return []byte(JwtKey), nil
	})

	if err != nil {
		if errors.Is(err, jwtGo.ErrSignatureInvalid) {
			// сделать обертку на ошибку в handler
			return "", "", fmt.Errorf("invalid in token")
		}

		return "", "", fmt.Errorf("could not check token. err: %w", err)
	}

	access, refresh, err := tokenService.GetNewTokensWithUpdate(claims.ID)
	if err != nil {
		return "", "", fmt.Errorf("could not create tokens. err: %w", err)
	}

	return access, refresh, nil
}

func (tokenService *TokenService) GetNewTokensWithUpdate(login string) (string, string, error) {
	// подумать куда впихнуть сикрет
	accessToken := tokenService.createToken(login, ExpireAccessMinutes)
	token := jwtGo.NewWithClaims(jwtGo.SigningMethodHS256, accessToken)
	accessString, err := token.SignedString([]byte(JwtKey))
	if err != nil {
		return "", "", fmt.Errorf("could not create tokens. err: %w", err)
	}

	refreshToken := tokenService.createToken(login, ExpireRefreshMinutes)
	token = jwtGo.NewWithClaims(jwtGo.SigningMethodHS256, refreshToken)
	refreshString, err := token.SignedString([]byte(JwtKey))
	if err != nil {
		return "", "", fmt.Errorf("could not create tokens. err: %w", err)
	}

	return accessString, refreshString, nil
}

func InitDefault() string {
	s := os.Getenv("jwt_secret")
	return s
}
