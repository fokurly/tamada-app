package service

import "context"

type AuthService struct {
	storage    Storage
	jwtService TokenService
}

func NewAuthService(storage Storage, jwtService TokenService) *AuthService {
	return &AuthService{
		storage:    storage,
		jwtService: jwtService,
	}
}

type Storage interface {
	NewUser(ctx context.Context, userData User) error
	GetUserInfo(ctx context.Context, login string) (*UserInfoResp, error)
	CheckUserExist(ctx context.Context, login, pass string) (bool, error)
	GetUserInfoByID(ctx context.Context, id int64) (*GetUserInfoByID, error)
	CheckUserPass(ctx context.Context, userID int64, pass string) (bool, error)
	DeleteUser(ctx context.Context, userID int64) error
	UpdateUserLogin(ctx context.Context, userID int64, newLogin string) error
	UpdateUserAvatar(ctx context.Context, userID int64, newAvatarID int64) error
	UpdatePassword(ctx context.Context, userID int64, newPassword string) error
	UpdateUserWalletCard(ctx context.Context, userID int64, cardNumber int64) error
	CreateUserWallet(ctx context.Context, userID int64) error
	GetUserWallet(ctx context.Context, userID int64) (*GetUserWalletOut, error)
	UpdateUserWalletOwner(ctx context.Context, userID int64, cardOwner string) error
	UpdateUserWalletPhone(ctx context.Context, userID int64, phone string) error
	UpdateUserWalletBank(ctx context.Context, userID int64, bank string) error
}

type TokenService interface {
	GetNewTokens(login string) (string, string, error)
	CheckToken(token string) (bool, error)
	RefreshToken(token string) (string, string, error)
}

//type ProcessAuth interface {
//	Login() error
//	Register(user User) error
//	GetTokensForUser(login string)
//}
