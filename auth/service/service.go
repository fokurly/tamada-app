package service

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func (a *AuthService) Ping(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, fmt.Sprintf("ping"))
}

// SignIn получаем access + refresh token
func (a *AuthService) Login(ctx *gin.Context) {
	var user User

	if err := ctx.BindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	isCorrect, err := a.storage.CheckUserExist(ctx, user.Login, user.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}
	if !isCorrect {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("check input data. could not find user with such login and password"))
		return
	}

	// мапп ошибок добавить
	userInfo, err := a.storage.GetUserInfo(ctx, user.Login)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user. error: %v", err))
		return
	}

	if userInfo == nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("no user with login: %s", user.Login))
		return
	}

	accessToken, refreshToken, err := a.jwtService.GetNewTokens(user.Login)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not create tokens. error: %v", err))
		return
	}
	resp := &struct {
		AccessToken    string
		RefreshToken   string
		Login          string
		UserID         int64
		ProfileImageID int64
		HasDebitCard   bool
	}{
		AccessToken:    accessToken,
		RefreshToken:   refreshToken,
		Login:          user.Login,
		UserID:         userInfo.ID,
		ProfileImageID: userInfo.ProfileImageID,
		HasDebitCard:   userInfo.HasDebitCard,
	}
	ctx.JSON(http.StatusOK, resp)
}

func (a *AuthService) Register(ctx *gin.Context) {
	var user User

	if err := ctx.BindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.NewUser(ctx, user)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not register user. error: %v", err))
		return
	}

	userInfo, err := a.storage.GetUserInfo(ctx, user.Login)
	if err != nil {
		ctx.JSON(http.StatusNoContent, fmt.Sprintf("could not get user id. error: %v", err))
		return
	}

	if userInfo == nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not register user"))
		return
	}

	err = a.storage.CreateUserWallet(ctx, userInfo.ID)
	if err != nil {
		logrus.Debugf("could not create user wallet. error: %v", err)
	}
	// добавить выдачу рефреш токена
	// спросить лину что делать если зарегали но не смогли сделать токены
	accessToken, refreshToken, err := a.jwtService.GetNewTokens(user.Login)
	if err != nil {
		ctx.JSON(http.StatusNoContent, fmt.Sprintf("could not create tokens. error: %v", err))
		return
	}

	//rand.New(rand.NewSource(time.Now().Unix()))
	//event.CoverID = rand.Int63() % 4
	resp := &struct {
		AccessToken    string
		RefreshToken   string
		Login          string
		UserID         int64
		ProfileImageID int64
	}{
		AccessToken:    accessToken,
		RefreshToken:   refreshToken,
		Login:          user.Login,
		UserID:         userInfo.ID,
		ProfileImageID: 0, // переделать на рандом потом
	}
	ctx.JSON(http.StatusOK, resp)
}

const (
	authorizationHeader = "Authorization"
)

func (a *AuthService) CheckAuth(ctx *gin.Context) {
	header := ctx.GetHeader(authorizationHeader)
	if header == "" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}
	tokenParts := strings.Split(header, " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	if len(tokenParts[1]) == 0 {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	isActual, err := a.jwtService.CheckToken(tokenParts[1])
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, "could not check token")
		return
	}
	if !isActual {
		ctx.JSON(http.StatusUnauthorized, "token is expired")
		return
	}

	ctx.Next()
}

// RefreshToken только для refresh token; получаем новую пару access + refresh по refresh token
func (a *AuthService) RefreshToken(ctx *gin.Context) {
	header := ctx.GetHeader(authorizationHeader)
	if header == "" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}
	tokenParts := strings.Split(header, " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	if len(tokenParts[1]) == 0 {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	access, refresh, err := a.jwtService.RefreshToken(tokenParts[1])
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, fmt.Sprintf("could not refresh token. err: %v", err))
		return
	}

	resp := &struct {
		AccessToken  string
		RefreshToken string
	}{
		AccessToken:  access,
		RefreshToken: refresh,
	}
	ctx.JSON(http.StatusOK, resp)
}

func (a *AuthService) CheckAuthGateway(ctx *gin.Context) {
	header := ctx.GetHeader(authorizationHeader)
	if header == "" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}
	tokenParts := strings.Split(header, " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	if len(tokenParts[1]) == 0 {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	isActual, err := a.jwtService.CheckToken(tokenParts[1])
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, "could not check token")
		return
	}
	if !isActual {
		ctx.JSON(http.StatusUnauthorized, "token is expired")
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) GetStatusUnauthorized(ctx *gin.Context) {
	ctx.JSON(http.StatusUnauthorized, nil)
}

func (a *AuthService) DeleteUser(ctx *gin.Context) {
	var userInfo DeleteUserIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	ok, err := a.storage.CheckUserPass(ctx, userInfo.UserID, userInfo.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user with such login and pass. error: %v", err))
		return
	}
	if !ok {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("invalid in"))
		return
	}

	err = a.storage.DeleteUser(ctx, userInfo.UserID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not delete user. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) UpdateUserLogin(ctx *gin.Context) {
	var userInfo UpdateUserLoginIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	res, err := a.storage.GetUserInfo(ctx, userInfo.NewLogin)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user info. error: %v", err))
		return
	}

	if res != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("login is already taken"))
		return
	}

	err = a.storage.UpdateUserLogin(ctx, userInfo.UserID, userInfo.NewLogin)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update login. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) UpdateUserAvatar(ctx *gin.Context) {
	var userInfo UpdateUserAvatarIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateUserAvatar(ctx, userInfo.UserID, userInfo.NewAvatarID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update login. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) UpdateUserPassword(ctx *gin.Context) {
	var userInfo UpdateUserPasswordIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	ok, err := a.storage.CheckUserPass(ctx, userInfo.UserID, userInfo.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user with such login and pass. error: %v", err))
		return
	}
	if !ok {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("invalid in"))
		return
	}

	err = a.storage.UpdatePassword(ctx, userInfo.UserID, userInfo.NewPassword)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update password. error: %v", err))
		return
	}
	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) GetUserWallet(ctx *gin.Context) {
	var userInfo GetUserWalletIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	walletInfo, err := a.storage.GetUserWallet(ctx, userInfo.UserID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user wallet. error: %v", err))
		return
	}

	if walletInfo == nil {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ctx.JSON(http.StatusOK, walletInfo)
}

func (a *AuthService) UpdateUserWalletCard(ctx *gin.Context) {
	var userInfo UpdateUserWalletCardIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateUserWalletCard(ctx, userInfo.UserID, userInfo.Card)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update user wallet. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) UpdateUserWalletPhone(ctx *gin.Context) {
	var userInfo UpdateUserWalletPhoneIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateUserWalletPhone(ctx, userInfo.UserID, userInfo.Phone)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update user wallet. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) UpdateUserWalletBank(ctx *gin.Context) {
	var userInfo UpdateUserWalletBankIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateUserWalletBank(ctx, userInfo.UserID, userInfo.Bank)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update user wallet. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}

func (a *AuthService) UpdateUserWalletOwner(ctx *gin.Context) {
	var userInfo UpdateUserWalletOwnerIn
	if err := ctx.BindJSON(&userInfo); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	err := a.storage.UpdateUserWalletOwner(ctx, userInfo.UserID, userInfo.Owner)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not update user wallet. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}
