package service

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type GetUserInfoIn struct {
	UserIDs []int64 `json:"userIDs"`
}

type GetUserInfoByID struct {
	Login    string `json:"login"`
	AvatarID int64  `json:"avatarID" db:"profile_image_id"`
	UserID   int64  `json:"userID" db:"id"`
}

func (a *AuthService) GetUsersInfo(ctx *gin.Context) {
	var params GetUserInfoIn

	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	res := make([]GetUserInfoByID, 0, len(params.UserIDs))
	for i := range params.UserIDs {
		data, err := a.storage.GetUserInfoByID(ctx, params.UserIDs[i])
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get user info in db. error: %v", err))
			return
		}

		if data == nil {
			continue
		}
		res = append(res, *data)
	}

	ctx.JSON(http.StatusOK, res)
}
