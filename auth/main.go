package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"gitlab.com/fokurly/tamada-app/auth/jwt_tokens"
	"gitlab.com/fokurly/tamada-app/auth/service"
	"gitlab.com/fokurly/tamada-app/auth/storage"
)

func main() {
	mainCtx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	db, err := initDB()
	if err != nil {
		logrus.Panicf(fmt.Sprintf("could not open db. err: %v", err))
	}
	authStorage := storage.NewStorage(db)
	tokenService := jwt_tokens.NewTokenService(authStorage)
	authService := service.NewAuthService(authStorage, tokenService)

	router := gin.Default()
	router.POST("/register", authService.Register)
	router.POST("/login", authService.Login)
	router.GET("/ping", authService.Ping)
	router.GET("/auth_error", authService.GetStatusUnauthorized)
	router.GET("/check_auth_gateway", authService.CheckAuthGateway)
	router.POST("/get_users_info", authService.GetUsersInfo)
	router.POST("/delete_user", authService.DeleteUser)
	router.POST("/update_user_login", authService.UpdateUserLogin)
	router.POST("/update_user_avatar", authService.UpdateUserAvatar)
	router.POST("/update_user_password", authService.UpdateUserPassword)

	router.POST("/get_user_wallet", authService.GetUserWallet)

	router.POST("/update_user_wallet_card", authService.UpdateUserWalletCard)
	router.POST("/update_user_wallet_owner", authService.UpdateUserWalletOwner)
	router.POST("/update_user_wallet_phone", authService.UpdateUserWalletPhone)
	router.POST("/update_user_wallet_bank", authService.UpdateUserWalletBank)
	usersApi := router.Group("/api", authService.CheckAuth)
	{
		usersApi.GET("/refresh_token", authService.RefreshToken)
	}

	server := http.Server{
		Addr:    ":8081",
		Handler: router,
	}
	g, gCtx := errgroup.WithContext(mainCtx)
	g.Go(func() error {
		return server.ListenAndServe()
	})
	g.Go(func() error {
		<-gCtx.Done()
		return server.Shutdown(context.Background())
	})

	if err := g.Wait(); err != nil {
		logrus.Print("exit reason: %s \n", err)
	}
}

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		PrettyPrint:     true,
	})
}

func initDB() (*sqlx.DB, error) {
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	dbName := os.Getenv("POSTGRES_DB")
	if pass == "" {
		user = "postgres"
		pass = "pass"
		dbName = "postgres"
		// для локалки
		query := fmt.Sprintf("host=0.0.0.0 port=5432 user=%s password=%s dbname=%s sslmode=disable", user, pass, dbName)
		return sqlx.Open("postgres", query)
	}
	query := fmt.Sprintf("host=db user=%s password=%s dbname=%s sslmode=disable", user, pass, dbName)
	return sqlx.Open("postgres", query)
}
