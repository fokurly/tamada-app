package producer

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/sirupsen/logrus"
)

type KafkaProducer struct {
	Producer *kafka.Producer
	Topic    string
}

func NewKafkaProducer() *KafkaProducer {
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost:9094"})
	if err != nil {
		logrus.Panic(err)
	}

	broker := &KafkaProducer{
		Producer: p,
		Topic:    "delete",
	}

	broker.LookForDelivery()
	logrus.Info("Producer up!")
	return broker
}
