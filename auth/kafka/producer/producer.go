package producer

import (
	"fmt"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	jsoniter "github.com/json-iterator/go"
)

func (k *KafkaProducer) LookForDelivery() {
	go func() {
		for e := range k.Producer.Events() {
			switch ev := e.(type) {
			case *kafka.Message:
				if ev.TopicPartition.Error != nil {
					fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
				}
			}
		}
	}()
}

func (k *KafkaProducer) Publish(res UserToDelete) error {
	b, err := jsoniter.Marshal(res)
	if err != nil {
		return fmt.Errorf("coulf not marshal data for producing. error: %s", err.Error())
	}

	err = k.Producer.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &k.Topic, Partition: kafka.PartitionAny},
		Value:          b,
	}, nil)

	if err != nil {
		return fmt.Errorf("could not send msg to kafka. error: %s", err.Error())
	}
	k.Producer.Flush(15 * 10000)

	return nil
}

type UserToDelete struct {
	UserID int64 `json:"userID"`
}
