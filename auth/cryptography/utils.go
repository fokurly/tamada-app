package cryptography

import (
	"crypto/md5"
	"fmt"
)

func HashPassword(password string) string {
	salt := "salt"
	h := md5.New()
	h.Write([]byte(password))
	return fmt.Sprintf("%x", h.Sum([]byte(salt)))
}
