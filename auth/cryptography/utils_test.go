package cryptography

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_HashPassword(t *testing.T) {
	pass := HashPassword("12345")
	secodPass := HashPassword("12345")

	require.Equal(t, pass, secodPass)
}
