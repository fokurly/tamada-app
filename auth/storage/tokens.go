package storage

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func (s *Storage) InsertTokens(ctx context.Context, userLogin, accessToken, refreshToken, uuid string) error {
	queryInsert := `
		INSERT INTO token_info (
		        user_login,
		    	refresh_token,
		    	access_token
		)
		values (
				:user_login,
		        :refresh_token,
		        :access_token
		)
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"user_login":    userLogin,
			"refresh_token": refreshToken,
			"access_token":  accessToken,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't insert pair of tokens. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateTokens(ctx context.Context, login, refreshToken, accessToken, uuid string) error {
	queryUpdate := `
		UPDATE token_info
		SET 
		    access_token = :accessToken,
			refresh_token = :refreshToken,
		WHERE user_login = :login and uuid = :uuid
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"accessToken":  accessToken,
			"refreshToken": refreshToken,
			"user_login":   login,
			"uuid":         uuid,
		}
		result, ierr := tx.NamedExecContext(ctx, queryUpdate, payload)
		if ierr != nil {
			return fmt.Errorf("can't update tokens: %w", ierr)
		}

		rowsAffected, err := result.RowsAffected()
		if err != nil {
			return fmt.Errorf("could not check if hash updated. error: %w", err)
		}

		if rowsAffected == 0 {
			return fmt.Errorf("not found value to update")
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) CheckAccessToken(ctx context.Context, accessToken string) (bool, error) {
	queryGet := `
		SELECT count(*) FROM token_info WHERE access_token=:access_token
	`

	payload := map[string]any{
		"access_token": accessToken,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return false, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var countRaw int64
	if rows.Next() {
		err := rows.Scan(&countRaw)
		if err != nil {
			return false, fmt.Errorf("could not scan rows. error: %v", err)
		}

		if countRaw == 0 {
			return false, nil
		}
	}

	return true, nil
}
func (s *Storage) CheckRefreshToken(ctx context.Context, accessToken string) (bool, error) {
	queryGet := `
		SELECT count(*) FROM token_info WHERE refresh_token=:refresh_token
	`

	payload := map[string]any{
		"refresh_token": accessToken,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return false, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var countRaw int64
	if rows.Next() {
		err := rows.Scan(&countRaw)
		if err != nil {
			return false, fmt.Errorf("could not scan rows. error: %v", err)
		}

		if countRaw == 0 {
			return false, nil
		}
	}

	return true, nil
}
