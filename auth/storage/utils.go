package storage

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func WithTxx(ctx context.Context, txer Txerx, f func(ctx context.Context, tx *sqlx.Tx) error) error {
	tx, err := txer.Beginx()
	if err != nil {
		return fmt.Errorf("can't begin tx: %w", err)
	}
	defer func() {
		_ = tx.Rollback()
	}()

	if err = f(ctx, tx); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("can't commit tx: %w", err)
	}

	return nil
}
