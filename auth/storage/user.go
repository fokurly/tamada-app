package storage

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"

	"gitlab.com/fokurly/tamada-app/auth/cryptography"
	"gitlab.com/fokurly/tamada-app/auth/service"
)

func (s *Storage) NewUser(ctx context.Context, userData service.User) error {
	queryInsert := `
		INSERT INTO user_register_info (
		        login,
		    	password,
		    	email
		)
		values (
				:login,
		        :password,
		        :email
		)
	`
	pass := cryptography.HashPassword(userData.Password)

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"login":    userData.Login,
			"password": pass,
			"email":    userData.Email,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't insert new user. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

// продумать
func (s *Storage) CheckUserExist(ctx context.Context, login, pass string) (bool, error) {
	queryGet := `
		SELECT id FROM user_register_info WHERE login = :login and password = :password
	`
	pass = cryptography.HashPassword(pass)
	payload := map[string]any{
		"login":    login,
		"password": pass,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return false, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var ID int64
	if rows.Next() {
		err := rows.Scan(&ID)
		if err != nil {
			return false, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return true, nil
	}

	return false, nil
}

func (s *Storage) GetUserInfo(ctx context.Context, login string) (*service.UserInfoResp, error) {
	queryGet := `
		SELECT id, email, login, profile_image_id, has_debit_card FROM user_register_info WHERE login=:login
	`

	payload := map[string]any{
		"login": login,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var usersInfo []service.UserInfoResp
	if rows.Next() {
		var userInfo service.UserInfoResp
		err := rows.StructScan(&userInfo)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}
		usersInfo = append(usersInfo, userInfo)
	}

	if len(usersInfo) == 0 {
		return nil, nil
	}
	return &usersInfo[0], nil
}

func (s *Storage) GetUserInfoByID(ctx context.Context, id int64) (*service.GetUserInfoByID, error) {
	queryGet := `
		SELECT login, profile_image_id, id
		FROM user_register_info WHERE id=:userID
	`

	payload := map[string]any{
		"userID": id,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	if rows.Next() {
		var partiesGuest service.GetUserInfoByID
		err := rows.StructScan(&partiesGuest)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}
		return &partiesGuest, nil
	}

	return nil, nil
}

// продумать
func (s *Storage) CheckUserPass(ctx context.Context, userID int64, pass string) (bool, error) {
	queryGet := `
		SELECT id FROM user_register_info WHERE id = :userID and password = :password
	`
	pass = cryptography.HashPassword(pass)
	payload := map[string]any{
		"userID":   userID,
		"password": pass,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return false, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var ID int64
	if rows.Next() {
		err := rows.Scan(&ID)
		if err != nil {
			return false, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return true, nil
	}

	return false, nil
}

func (s *Storage) DeleteUser(ctx context.Context, userID int64) error {
	queryInsert := `
		DELETE FROM user_register_info WHERE id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID": userID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't insert new user. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateUserLogin(ctx context.Context, userID int64, newLogin string) error {
	queryInsert := `
		UPDATE user_register_info SET login=:newLogin WHERE id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID":   userID,
			"newLogin": newLogin,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update login. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateUserAvatar(ctx context.Context, userID int64, newAvatarID int64) error {
	queryInsert := `
		UPDATE user_register_info SET profile_image_id=:newAvatarID WHERE id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID":      userID,
			"newAvatarID": newAvatarID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't avatar login. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdatePassword(ctx context.Context, userID int64, newPassword string) error {
	queryInsert := `
		UPDATE user_register_info SET password=:pass WHERE id=:userID
	`

	pass := cryptography.HashPassword(newPassword)
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID": userID,
			"pass":   pass,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't edit password. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateUserWalletCard(ctx context.Context, userID int64, cardNumber int64) error {
	queryInsert := `
		UPDATE user_wallet SET card_number=:card WHERE user_id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID": userID,
			"card":   cardNumber,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update card number. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateUserWalletBank(ctx context.Context, userID int64, bank string) error {
	queryInsert := `
		UPDATE user_wallet SET bank=:bank WHERE user_id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID": userID,
			"bank":   bank,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update bank. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateUserWalletPhone(ctx context.Context, userID int64, phone string) error {
	queryInsert := `
		UPDATE user_wallet SET phone_number=:phone WHERE user_id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID": userID,
			"phone":  phone,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update phone. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) UpdateUserWalletOwner(ctx context.Context, userID int64, cardOwner string) error {
	queryInsert := `
		UPDATE user_wallet SET card_owner=:cardOwner WHERE user_id=:userID
	`

	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID":    userID,
			"cardOwner": cardOwner,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't update phone. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) CreateUserWallet(ctx context.Context, userID int64) error {
	queryInsert := `
		INSERT INTO user_wallet (user_id) 
		values (
		        :userID                   
		)
	`
	err := WithTxx(ctx, s.dbx, func(ctx context.Context, tx *sqlx.Tx) error {
		payload := map[string]any{
			"userID": userID,
		}

		if _, ierr := tx.NamedExecContext(ctx, queryInsert, payload); ierr != nil {
			return fmt.Errorf("can't insert user wallet. error: %w", ierr)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("txer failed: %w", err)
	}

	return nil
}

func (s *Storage) GetUserWallet(ctx context.Context, userID int64) (*service.GetUserWalletOut, error) {
	queryGet := `
		SELECT id, user_id, card_number, card_owner, phone_number, bank FROM user_wallet WHERE user_id=:userID
	`
	payload := map[string]any{
		"userID": userID,
	}

	rows, err := s.dbx.NamedQueryContext(ctx, queryGet, payload)
	if err != nil {
		return nil, fmt.Errorf("can't get rows: %w", err)
	}
	defer func() {
		_ = rows.Close()
	}()

	var res service.GetUserWalletOut
	if rows.Next() {
		err := rows.StructScan(&res)
		if err != nil {
			return nil, fmt.Errorf("could not scan rows. error: %v", err)
		}

		return &res, nil
	}

	return nil, nil
}
