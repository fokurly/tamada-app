package clients

import "net/http"

type UserClient struct {
	Url     string
	httpCli *http.Client
}

func NewUserClient(url string) *UserClient {
	return &UserClient{Url: url, httpCli: &http.Client{}}
}
