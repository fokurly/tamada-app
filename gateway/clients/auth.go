package clients

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"

	jsoniter "github.com/json-iterator/go"

	"gitlab.com/fokurly/tamada-app/gateway/common"
)

type AuthClient struct {
	Url     string
	httpCli *http.Client
}

func NewAuthClient(url string) *AuthClient {
	return &AuthClient{
		Url: url, httpCli: &http.Client{},
	}
}

func (a *AuthClient) Ping(ctx context.Context) ([]byte, int, error) {
	url := fmt.Sprintf(a.Url + "/ping")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, 0, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return nil, 0, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonData, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	respCode := resp.StatusCode

	return jsonData, respCode, nil
}

func (a *AuthClient) Login(ctx context.Context, in common.LoginIn) ([]byte, int, error) {
	url := fmt.Sprintf(a.Url + "/login")
	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	resp, err := a.httpCli.Do(req)
	if err != nil {
		return nil, 0, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, nil
}

func (a *AuthClient) Register(ctx context.Context, in common.RegisterIn) ([]byte, int, error) {
	url := fmt.Sprintf(a.Url + "/register")

	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func (a *AuthClient) RefreshToken(ctx context.Context, token string) ([]byte, int, error) {
	url := fmt.Sprintf(a.Url + "/api/refresh_token")
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	req.Header.Set("Authorization", token)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}
	return jsonResp, resp.StatusCode, nil
}

func (a *AuthClient) CheckToken(ctx context.Context, token string) (int, error) {
	url := fmt.Sprintf(a.Url + "/check_auth_gateway")
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	req.Header.Set("Authorization", token)
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	return resp.StatusCode, nil
}

func (a *AuthClient) GetUsersInfo(ctx context.Context, in common.GetUserInfoByIDIn) ([]common.GetUserInfoByIDOut, error) {
	url := fmt.Sprintf(a.Url + "/get_users_info")
	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)

	if err != nil {
		return nil, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return nil, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	var res []common.GetUserInfoByIDOut
	if err := jsoniter.Unmarshal(jsonResp, &res); err != nil {
		return nil, fmt.Errorf("could not unmarshal data. error: %w", err)
	}

	return res, nil
}

func (a *AuthClient) DeleteUser(ctx context.Context, in common.DeleteUserIn) (int, error) {
	url := fmt.Sprintf(a.Url + "/delete_user")
	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := a.httpCli.Do(req)
	if err != nil {
		return http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	return resp.StatusCode, nil
}

func (e *AuthClient) UpdateUserFields(ctx context.Context, jsonData []byte, endpoint string) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + endpoint)

	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}
