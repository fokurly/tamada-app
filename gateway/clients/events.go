package clients

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"path/filepath"

	jsoniter "github.com/json-iterator/go"

	"gitlab.com/fokurly/tamada-app/gateway/common"
)

type EventClient struct {
	Url     string
	httpCli *http.Client
}

func NewEventClient(url string) *EventClient {
	return &EventClient{Url: url, httpCli: &http.Client{}}
}

func (e *EventClient) CreateEvent(ctx context.Context, in common.CreateEventIn) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + "/create_event")

	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func (e *EventClient) GetEvent(ctx context.Context, in common.GetEventIn) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + "/get_event")

	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func (e *EventClient) GetUserEvents(ctx context.Context, in common.GetUserEventsIn) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + "/get_user_parties")

	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func (e *EventClient) GetPartiesGuests(ctx context.Context, in common.GetPartiesGuestsIn) ([]common.GetPartiesGuestsClientResp, error) {
	url := fmt.Sprintf(e.Url + "/get_parties_guests")

	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	var res []common.GetPartiesGuestsClientResp
	if err := jsoniter.Unmarshal(jsonResp, &res); err != nil {
		return nil, fmt.Errorf("could not unmarshal resp from event service. error: %w", err)
	}

	return res, nil
}

func (e *EventClient) UpdateEventCoverID(ctx context.Context, in common.UpdateEventCoverIDIn) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + "/update_event_cover_id")

	jsonData, err := jsoniter.Marshal(in)
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func (e *EventClient) UpdateEventFields(ctx context.Context, jsonData []byte, endpoint string) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + endpoint)

	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func (e *EventClient) UploadFile(ctx context.Context, name, typee string, sum float64, userID, partyID int64, endpoint string) ([]byte, int, error) {
	url := fmt.Sprintf(e.Url + endpoint)

	reqModel := common.UploadReceiptIn{
		Name:    name,
		Sum:     sum,
		Type:    typee,
		UserID:  userID,
		PartyID: partyID,
	}

	jsonData, err := jsoniter.Marshal(reqModel)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not marshal model. error: %v", err)
	}
	var body io.Reader = bytes.NewReader(jsonData)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}

	resp, err := e.httpCli.Do(req)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not make request. error: %s", err.Error())
	}
	defer resp.Body.Close()

	jsonResp, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("could not read result from body. error: %s", err.Error())
	}

	return jsonResp, resp.StatusCode, err
}

func test() {
	fileDir, _ := os.Getwd()
	fileName := "upload-file.txt"
	filePath := path.Join(fileDir, fileName)

	file, _ := os.Open(filePath)
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filepath.Base(file.Name()))
	io.Copy(part, file)
	writer.Close()

	r, _ := http.NewRequest("POST", "http://example.com", body)
	r.Header.Add("Content-Type", writer.FormDataContentType())
	client := &http.Client{}
	client.Do(r)
}
