package common

type LoginIn struct {
	Login    string
	Password string
}

type LoginOut struct {
	AccessToken    string
	RefreshToken   string
	Login          string
	UserID         int64
	ProfileImageID int64
	HasDebitCard   bool
}

type RegisterIn struct {
	Login    string
	Password string
	Email    string
}

type RegisterOut struct {
	AccessToken    string
	RefreshToken   string
	Login          string
	UserID         int64
	ProfileImageID int64
}

type RefreshTokenOut struct {
	AccessToken  string
	RefreshToken string
}

const AuthorizationHeader = "Authorization"

type GetUserInfoByIDOut struct {
	Login    string `json:"login"`
	UserID   int64  `json:"userID"`
	AvatarID int64  `json:"avatarID"`
}

type GetUserInfoByIDIn struct {
	UserIDs []int64 `json:"userIDs"`
}

type DeleteUserIn struct {
	UserID   int64  `json:"userID" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type UpdateUserLoginIn struct {
	UserID   int64  `json:"userID" binding:"required"`
	NewLogin string `json:"newLogin" binding:"required"`
}

type UpdateUserAvatarIn struct {
	UserID      int64 `json:"userID" binding:"required"`
	NewAvatarID int64 `json:"newAvatarID" binding:"required"`
}

type UpdateUserPasswordIn struct {
	UserID      int64  `json:"userID" binding:"required"`
	Password    string `json:"password" binding:"required"`
	NewPassword string `json:"newPassword" binding:"required"`
}

type GetUserWalletIn struct {
	UserID int64 `json:"userID"`
}

type UpdateUserWalletCardIn struct {
	UserID int64 `json:"userID"`
	Card   int64 `json:"cardNumber"`
}

type UpdateUserWalletPhoneIn struct {
	UserID int64  `json:"userID"`
	Phone  string `json:"phoneNumber"`
}

type UpdateUserWalletBankIn struct {
	UserID int64  `json:"userID"`
	Bank   string `json:"bank"`
}

type UpdateUserWalletOwnerIn struct {
	UserID int64  `json:"userID"`
	Owner  string `json:"cardOwner"`
}

type GetUserWalletOut struct {
	UserID      int64  `json:"userID"  db:"user_id"`
	WalletID    int64  `json:"walletID" db:"id"`
	CardOwner   string `json:"cardOwner" db:"card_owner"`
	CardNumber  string `json:"cardNumber" db:"card_number"`
	PhoneNumber string `json:"phoneNumber" db:"phone_number"`
	Bank        string `json:"bank" db:"bank"`
}
