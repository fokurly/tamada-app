package common

type CreateEventIn struct {
	UserCreatorID     int64  `json:"userCreatorID" binding:"required"`
	Name              string `json:"name"`
	StartTime         string `json:"startTime"`
	EndTime           string `json:"endTime"`
	Address           string `json:"address"`
	AddressLink       string `json:"addressLink"`
	AddressAdditional string `json:"addressAdditional"`
	IsExpenses        int64  `json:"isExpenses"` // 0-3 4 разных состояния, клиент засылает
	Important         string `json:"important"`
	Theme             string `json:"theme"`
	DressCode         string `json:"dressCode"`
	MoodboadLink      string `json:"moodboadLink"`
	CoverID           int64  `json:"coverID"` // обложка, рандом изначально
}

type GetUserEventsIn struct {
	UserID int64 `json:"userID" binding:"required"`
}

type GetUserPartiesOut struct {
	PartyID   int64  `json:"partyID"`
	PartyName string `json:"name"`
	CoverID   int64  `json:"coverID"`
	IsManager bool   `json:"isManager"`
	IsNew     bool   `json:"isNew"` // 3 дня после создания
}

type GetEventOut struct {
	UserCreatorID     int64  `json:"userCreatorID"`
	PartyID           int64  `json:"partyID"`
	Name              string `json:"name"`
	StartTime         string `json:"startTime"`
	EndTime           string `json:"endTime"`
	Address           string `json:"address"`
	AddressLink       string `json:"addressLink"`
	AddressAdditional string `json:"addressAdditional"`
	IsExpenses        int64  `json:"isExpenses"` // 0-3 4 разных состояния, клиент засылает
	Important         string `json:"important"`
	Theme             string `json:"theme"`
	DressCode         string `json:"dressCode"`
	MoodboadLink      string `json:"moodboadLink"`
	CoverID           int64  `json:"coverID"` // обложка, рандом изначально
}

type GetEventIn struct {
	PartyID int64 `json:"party_id" binding:"required"`
}

type GetPartiesGuestsIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartiesGuestsClientResp struct {
	UserID int64
	Role   string
	Status string
}

type GetPartiesGuestsOut struct {
	UserID   int64
	Role     string
	Status   string
	AvatarID int64
	Login    string
}

type DeleteUserFromPartyIn struct {
	UserID     int64 `json:"userToDelete" binding:"required"` // юзер которого удаляют
	ActionFrom int64 `json:"actionFrom" binding:"required"`   // id юзера, который удаляет
	PartyID    int64 `json:"partyID" binding:"required"`
}

type AddUserToPartyIn struct {
	UserID  int64 `json:"userID" binding:"required"` // юзер которого удаляют
	PartyID int64 `json:"partyID" binding:"required"`
}

type UpdateUserRoleOnPartyIn struct {
	UserID     int64  `json:"userID" binding:"required"`
	ActionFrom int64  `json:"actionFrom" binding:"required"`
	PartyID    int64  `json:"partyID" binding:"required"`
	NewRole    string `json:"newRole" binding:"required"` // available confirmed, uncertain, unconfirmed
}

type UpdateEventStartTimeIn struct {
	ActionFrom   int64  `json:"actionFrom" binding:"required"`
	PartyID      int64  `json:"partyID" binding:"required"`
	NewStartTime string `json:"newStartTime" binding:"required"`
}

type UpdateEventEndTimeIn struct {
	ActionFrom int64  `json:"actionFrom" binding:"required"`
	PartyID    int64  `json:"partyID" binding:"required"`
	NewEndTime string `json:"newEndTime" binding:"required"`
}

type UpdateEventAddressIn struct {
	ActionFrom int64  `json:"actionFrom" binding:"required"`
	PartyID    int64  `json:"partyID" binding:"required"`
	Address    string `json:"address" binding:"required"`
}

type UpdateEventAddressLinkIn struct {
	ActionFrom  int64  `json:"actionFrom" binding:"required"`
	PartyID     int64  `json:"partyID" binding:"required"`
	AddressLink string `json:"addressLink" binding:"required"`
}

type UpdateEventAddressAdditionalInfoIn struct {
	ActionFrom            int64  `json:"actionFrom" binding:"required"`
	PartyID               int64  `json:"partyID" binding:"required"`
	AddressAdditionalInfo string `json:"addressAdditionalInfo" binding:"required"`
}

type UpdateEventIsExpensesIn struct {
	ActionFrom int64 `json:"actionFrom" binding:"required"`
	PartyID    int64 `json:"partyID" binding:"required"`
	IsExpenses int64 `json:"isExpenses" binding:"required"`
}

type UpdateEventImportantIn struct {
	ActionFrom int64  `json:"actionFrom" binding:"required"`
	PartyID    int64  `json:"partyID" binding:"required"`
	Important  string `json:"important" binding:"required"`
}

type UpdateEventThemeIn struct {
	ActionFrom int64  `json:"actionFrom" binding:"required"`
	PartyID    int64  `json:"partyID" binding:"required"`
	Theme      string `json:"theme" binding:"required"`
}

type UpdateEventDressCodeIn struct {
	ActionFrom int64  `json:"actionFrom" binding:"required"`
	PartyID    int64  `json:"partyID" binding:"required"`
	DressCode  string `json:"dressCode" binding:"required"`
}

type UpdateEventMoodboardLinkIn struct {
	ActionFrom    int64  `json:"actionFrom" binding:"required"`
	PartyID       int64  `json:"partyID" binding:"required"`
	MoodboardLink string `json:"moodboardLink" binding:"required"`
}

type UpdateEventCoverIDIn struct {
	ActionFrom int64 `json:"actionFrom" binding:"required"`
	PartyID    int64 `json:"partyID" binding:"required"`
	CoverID    int64 `json:"coverID" binding:"required"`
}

type AddListToEventIn struct {
	PartyID int64  `json:"partyID" binding:"required"`
	Type    string `json:"name" binding:"required,oneof=EMPTY TODO BUY WISHLIST"`
}

type AddListToEventOut struct {
	ListID int64 `json:"listID"`
}

type UpdateListVisibilityIn struct {
	ListID    int64 `json:"listID" binding:"required"`
	IsVisible bool  `json:"isVisible"`
}

type TaskCreateIn struct {
	Name string `json:"name" binding:"required"`
}

type AddTasksToListIn struct {
	ListID int64          `json:"listID" binding:"required"`
	Tasks  []TaskCreateIn `json:"tasks" binding:"required"`
}

type UpdateTaskStatusIn struct {
	TaskID int64 `json:"taskID" binding:"required"`
	IsDone bool  `json:"isDone"`
}

type UpdateTaskNameIn struct {
	TaskID  int64  `json:"taskID" binding:"required"`
	NewName string `json:"newName" binding:"required"`
}

type GetListInfoIn struct {
	ListID  int64 `json:"listID" binding:"required"`
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetListInfoOut struct {
	ListID    int64      `json:"listID"`
	Type      string     `json:"type"`
	PartyID   int64      `json:"partyID"`
	IsVisible string     `json:"isVisible"`
	Tasks     []TaskInfo `json:"tasks"`
}

type TaskInfo struct {
	TaskID int64  `json:"taskID"`
	Name   string `json:"name"`
	IsDone bool   `json:"isDone"`
}

type GetPartyListsIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartyListsOut struct {
	Type      string `json:"type"`
	PartyID   int64  `json:"party_id"`
	ListID    int64  `json:"id"`
	IsVisible string `json:"isVisible"`
}

type DeleteListIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
	ListID  int64 `json:"listID" binding:"required"`
}

type UpdateListTypeIn struct {
	ListID  int64  `json:"listID" binding:"required"`
	NewType string `json:"newType" binding:"required,oneof=EMPTY TODO BUY WISHLIST"`
}

type UploadReceiptOut struct {
	ExpensesID int64 `json:"expensesID"`
}
type UploadReceiptIn struct {
	Name    string  `json:"name" binding:"required"`
	Sum     float64 `json:"sum" binding:"required"`
	UserID  int64   `json:"userID" binding:"required"`
	PartyID int64   `json:"partyID" binding:"required"`
	Type    string  `json:"type" binding:"required,oneof=SUM DEBT"`
}

type GetUserReceiptIn struct {
	UserID    int64 `json:"userID" binding:"required"`
	PartyID   int64 `json:"partyID" binding:"required"`
	ExpenseID int64 `json:"expenseID" binding:"required"`
}

type LoadFinanceStateIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type LoadFinanceStateOut struct {
	IsExpenses int64 `json:"financeState" binding:"required"`
}

type UpdateFinanceStateIn struct {
	PartyID      int64 `json:"partyID" binding:"required"`
	FinanceState int64 `json:"financeState" binding:"required"`
	UserID       int64 `json:"userID" binding:"required"`
}

type GetUserExpenseInfoByIDIn struct {
	UserID    int64 `json:"userID" binding:"required"`
	PartyID   int64 `json:"partyID" binding:"required"`
	ExpenseID int64 `json:"expenseID" binding:"required"`
}

type GetUserExpenseInfoByIDOut struct {
	PartyID  int64   `json:"partyID" db:"party_id"`
	UserID   int64   `json:"userID" db:"user_id"`
	Name     string  `json:"name"  db:"name"`
	Sum      float64 `json:"sum" db:"sum"`
	Type     string  `json:"type" db:"type"`
	Expenses int64   `json:"expensesID" db:"id"`
}

type GetPartySummaryExpensesIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartySummaryExpensesOut struct {
	TotalSum float64 `json:"totalSum"`
}

type UserExpenses struct {
	PartyID  int64   `json:"partyID"`
	UserID   int64   `json:"userID"`
	Name     string  `json:"name"`
	Sum      float64 `json:"sum"`
	Type     string  `json:"type"`
	Expenses int64   `json:"expensesID"`
}

type GetAllUserReceiptIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
	UserID  int64 `json:"userID" binding:"required"`
}

type UpdateEventNameIn struct {
	PartyID    int64  `json:"partyID" binding:"required"`
	Name       string `json:"name" binding:"required"`
	ActionFrom int64  `json:"actionFrom" binding:"required"`
}

type GetInviteLinkIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetInviteLinkOut struct {
	Link string `json:"link"`
}

type GetPartyWalletIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartyWalletOut struct {
	PartyID     int64  `json:"partyID"`
	WalletID    int64  `json:"walletID"`
	CardOwner   string `json:"cardOwner"`
	CardNumber  string `json:"cardNumber"`
	PhoneNumber string `json:"phoneNumber"`
	Bank        string `json:"bank"`
}

type UpdatePartyWalletOwnerIn struct {
	PartyID int64  `json:"partyID" binding:"required"`
	Owner   string `json:"owner" binding:"required"`
}

type UpdatePartyWalletBankIn struct {
	PartyID int64  `json:"partyID" binding:"required"`
	Bank    string `json:"bank" binding:"required"`
}

type UpdatePartyWalletPhoneIn struct {
	PartyID     int64  `json:"partyID" binding:"required"`
	PhoneNumber string `json:"phoneNumber" binding:"required"`
}

type UpdatePartyWalletCardIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
	Card    int64 `json:"cardNumber" binding:"required"`
}

type CalculateExpensesIn struct {
	UserID  int64 `json:"userID" binding:"required"`
	PartyID int64 `json:"partyID" binding:"required"`
}

type CalculateExpensesOut struct {
	ExpenseForUser string  `json:"expenseForUser"`
	UserDebtString string  `json:"userDebtString"`
	UserDebt       float64 `json:"userDebt"`
}

type UploadReceiptInfoSumIn struct {
	NewSum    float64 `json:"newSum" binding:"required"`
	UserID    int64   `json:"userID" binding:"required"`
	PartyID   int64   `json:"partyID" binding:"required"`
	ExpenseID int64   `json:"expenseID" binding:"required"`
}

type UploadReceiptInfoNameIn struct {
	NewName   string `json:"newName" binding:"required"`
	UserID    int64  `json:"userID" binding:"required"`
	PartyID   int64  `json:"partyID" binding:"required"`
	ExpenseID int64  `json:"expenseID" binding:"required"`
}

type UpdatePartyExpensesDeadlineIn struct {
	PartyID          int64  `json:"partyID" binding:"required"`
	ExpensesDeadline string `json:"ExpensesDeadline" binding:"omitempty"`
}

type GetPartyExpensesDeadlineIn struct {
	PartyID int64 `json:"partyID" binding:"required"`
}

type GetPartyExpensesDeadlineOut struct {
	DeadlineExpenses string `json:"deadlineExpenses" db:"deadline_expenses"`
}

type SendNotificationIn struct {
	UserID int64  `json:"userID" binding:"required"`
	Title  string `json:"title" binding:"required"`
	Body   string `json:"body" binding:"required"`
}

type DeleteTaskFromListIn struct {
	ListID int64 `json:"listID" binding:"required"`
	TaskID int64 `json:"taskID" binding:"required"`
}
