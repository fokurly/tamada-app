package service

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
	"github.com/unidoc/unipdf/v3/model"

	"gitlab.com/fokurly/tamada-app/gateway/common"
)

func (g *Gateway) Ping(ctx *gin.Context) {
	body, _, err := g.AuthClient.Ping(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do req for auth service. err: %s", err))
		return
	}

	ctx.String(http.StatusOK, string(body))
}

// @Summary Login
// @Tags auth
// @Description авторизация в аккаунте
// @ID login-acc
// @Accept  json
// @Produce  json
// @Param input body common.LoginIn true "account info"
// @Success 200 {object} common.LoginOut
// @Router /login [post]
func (g *Gateway) Login(ctx *gin.Context) {
	var params common.LoginIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.Login(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary DeleteUser
// @Security ApiKeyAuth
// @Tags auth
// @Description Удаление пользователя
// @ID delete-user
// @Accept  json
// @Produce  json
// @Param input body common.DeleteUserIn true "event info"
// @Success 204
// @Router /api/delete_user [post]
func (g *Gateway) DeleteUser(ctx *gin.Context) {
	// TODO добавить удаление юзера со всех меро и тд
	var params common.DeleteUserIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	code, err := g.AuthClient.DeleteUser(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not delete user. error: %v", err))
		return
	}

	ctx.JSON(code, nil)
}

// @Summary Register
// @Tags auth
// @Description регистрация в аккаунте
// @ID register-acc
// @Accept  json
// @Produce  json
// @Param input body common.RegisterIn true "account info"
// @Success 200 {object} common.RegisterOut
// @Router /register [post]
func (g *Gateway) Register(ctx *gin.Context) {
	var params common.RegisterIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.Register(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

func (g *Gateway) RefreshToken(ctx *gin.Context) {
	token := ctx.GetHeader(common.AuthorizationHeader)
	data, code, err := g.AuthClient.RefreshToken(ctx, token)
	if err != nil {
		ctx.JSON(code, nil)
		return
	}

	ctx.String(code, string(data))
}

// @Summary CreateEvent
// @Security ApiKeyAuth
// @Tags event
// @Description Создание мероприятия пользователя
// @ID create-event
// @Accept  json
// @Produce  json
// @Param input body common.CreateEventIn true "event info"
// @Success 204
// @Router /api/create_event [post]
func (g *Gateway) CreateEvent(ctx *gin.Context) {
	var params common.CreateEventIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.CreateEvent(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetEvent
// @Security ApiKeyAuth
// @Tags event
// @Description Получение информации о мероприятии
// @ID get-event
// @Accept  json
// @Produce  json
// @Param input body common.GetEventIn true "event info"
// @Success 200 {object} common.GetEventOut
// @Router /api/get_event [post]
func (g *Gateway) GetEvent(ctx *gin.Context) {
	var params common.GetEventIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.GetEvent(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetUserEvents
// @Security ApiKeyAuth
// @Tags event
// @Description Получение мероприятий пользователя
// @ID get-user-events
// @Accept  json
// @Produce  json
// @Param input body common.GetUserEventsIn true "event info"
// @Success 200 {object} []common.GetUserPartiesOut
// @Router /api/get_user_parties [post]
func (g *Gateway) GetUserEvents(ctx *gin.Context) {
	var params common.GetUserEventsIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.GetUserEvents(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetPartiesGuests
// @Security ApiKeyAuth
// @Tags event
// @Description Получение списка гостей мероприятия
// @ID Get-Parties-Guests
// @Accept  json
// @Produce  json
// @Param input body common.GetPartiesGuestsIn true "event info"
// @Success 200 {object} common.GetPartiesGuestsOut
// @Router /api/get_parties_guests [post]
func (g *Gateway) GetPartiesGuests(ctx *gin.Context) {
	var params common.GetPartiesGuestsIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, err := g.EventClient.GetPartiesGuests(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get info. error: %v", err))
		return
	}

	if data == nil {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	ids := make([]int64, 0, len(data))
	for i := range data {
		ids = append(ids, data[i].UserID)
	}

	userData, err := g.AuthClient.GetUsersInfo(ctx, common.GetUserInfoByIDIn{
		UserIDs: ids,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get info. error: %v", err))
		return
	}

	if userData == nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("nil data from user info"))
		return
	}

	var res = make(map[int64]common.GetPartiesGuestsOut)
	for i := range ids {
		res[ids[i]] = common.GetPartiesGuestsOut{}
	}

	for i := range userData {
		if val, ok := res[userData[i].UserID]; ok {
			val.UserID = userData[i].UserID
			val.Login = userData[i].Login
			val.AvatarID = userData[i].AvatarID
			res[userData[i].UserID] = val
		}
	}

	for i := range data {
		if val, ok := res[data[i].UserID]; ok {
			val.Role = data[i].Role
			val.Status = data[i].Status
			res[userData[i].UserID] = val
		}
	}

	final := make([]common.GetPartiesGuestsOut, 0, len(data))
	for _, v := range res {
		final = append(final, v)
	}

	ctx.JSON(http.StatusOK, final)
}

// @Summary UpdateEventCoverID
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление обложки меро
// @ID update-cover-id
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventCoverIDIn true "event info"
// @Success 204
// @Router /api/update_event_moodboard_link [post]
func (g *Gateway) UpdateEventCoverID(ctx *gin.Context) {
	var params common.UpdateEventCoverIDIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventCoverID(ctx, params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventMoodboardLink
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление ссылки на мудборд
// @ID update-moodboard
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventMoodboardLinkIn true "event info"
// @Success 204
// @Router /api/update_event_moodboard_link [post]
func (g *Gateway) UpdateEventMoodboardLink(ctx *gin.Context) {
	var params common.UpdateEventMoodboardLinkIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_moodboard_link")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdatePartyWalletOwner
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление владельца кошелька мероприятия
// @ID update-party-wallet-owner
// @Accept  json
// @Produce  json
// @Param input body common.UpdatePartyWalletOwnerIn true "event info"
// @Success 204
// @Router /api/update_party_wallet_owner [post]
func (g *Gateway) UpdatePartyWalletOwner(ctx *gin.Context) {
	var params common.UpdatePartyWalletOwnerIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_party_wallet_owner")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdatePartyWalletBank
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление банка кошелька мероприятия
// @ID update-party-wallet-bank
// @Accept  json
// @Produce  json
// @Param input body common.UpdatePartyWalletBankIn true "event info"
// @Success 204
// @Router /api/update_party_wallet_bank [post]
func (g *Gateway) UpdatePartyWalletBank(ctx *gin.Context) {
	var params common.UpdatePartyWalletBankIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_party_wallet_bank")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdatePartyWalletPhone
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление телефона кошелька мероприятия
// @ID update-party-wallet-phone
// @Accept  json
// @Produce  json
// @Param input body common.UpdatePartyWalletPhoneIn true "event info"
// @Success 204
// @Router /api/update_party_wallet_phone [post]
func (g *Gateway) UpdatePartyWalletPhone(ctx *gin.Context) {
	var params common.UpdatePartyWalletPhoneIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_party_wallet_phone")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdatePartyWalletCard
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление карты кошелька мероприятия
// @ID update-party-wallet-card
// @Accept  json
// @Produce  json
// @Param input body common.UpdatePartyWalletCardIn true "event info"
// @Success 204
// @Router /api/update_party_wallet_card [post]
func (g *Gateway) UpdatePartyWalletCard(ctx *gin.Context) {
	var params common.UpdatePartyWalletCardIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_party_wallet_card")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventDressCode
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление дресс-кода
// @ID update-dress-code
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventDressCodeIn true "event info"
// @Success 204
// @Router /api/update_event_dress_code [post]
func (g *Gateway) UpdateEventDressCode(ctx *gin.Context) {
	var params common.UpdateEventDressCodeIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_dress_code")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventTheme
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление тематики
// @ID update-theme
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventThemeIn true "event info"
// @Success 204
// @Router /api/update_event_theme [post]
func (g *Gateway) UpdateEventTheme(ctx *gin.Context) {
	var params common.UpdateEventThemeIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_theme")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventImportant
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление включены ли ты траты на меро
// @ID update-important
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventImportantIn true "event info"
// @Success 204
// @Router /api/update_event_important [post]
func (g *Gateway) UpdateEventImportant(ctx *gin.Context) {
	var params common.UpdateEventImportantIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_important")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventIsExpenses
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление включены ли ты траты на меро
// @ID update-is-expenses
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventIsExpensesIn true "event info"
// @Success 204
// @Router /api/update_event_is_expenses [post]
func (g *Gateway) UpdateEventIsExpenses(ctx *gin.Context) {
	var params common.UpdateEventIsExpensesIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_is_expenses")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventAddressAdditionalInfo
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление доп информации адреса
// @ID update-address-additional-info
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventAddressAdditionalInfoIn true "event info"
// @Success 204
// @Router /api/update_event_address_additional [post]
func (g *Gateway) UpdateEventAddressAdditionalInfo(ctx *gin.Context) {
	var params common.UpdateEventAddressAdditionalInfoIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_address_additional")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventAddressLink
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление ссылки на адрес мероприятия
// @ID update-address-link
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventAddressLinkIn true "event info"
// @Success 204
// @Router /api/update_event_address_link [post]
func (g *Gateway) UpdateEventAddressLink(ctx *gin.Context) {
	var params common.UpdateEventAddressLinkIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_address_link")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventAddress
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление адреса мероприятия
// @ID update-address
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventAddressIn true "event info"
// @Success 204
// @Router /api/update_event_address [post]
func (g *Gateway) UpdateEventAddress(ctx *gin.Context) {
	var params common.UpdateEventAddressIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_address")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventEndTime
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление окончания времени мероприятия
// @ID update-end-time
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventEndTimeIn true "event info"
// @Success 204
// @Router /api/update_event_end_time [post]
func (g *Gateway) UpdateEventEndTime(ctx *gin.Context) {
	var params common.UpdateEventEndTimeIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_end_time")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventStartTime
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление начала времени мероприятия
// @ID update-start-time
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventStartTimeIn true "event info"
// @Success 204
// @Router /api/update_event_start_time [post]
func (g *Gateway) UpdateEventStartTime(ctx *gin.Context) {
	var params common.UpdateEventStartTimeIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_start_time")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateEventName
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление названия мероприятия
// @ID update-event-name
// @Accept  json
// @Produce  json
// @Param input body common.UpdateEventNameIn true "event info"
// @Success 204
// @Router /api/update_event_name [post]
func (g *Gateway) UpdateEventName(ctx *gin.Context) {
	var params common.UpdateEventNameIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_event_name")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary DeleteUserFromParty
// @Security ApiKeyAuth
// @Tags event
// @Description Удаление пользователя с мероприятия
// @ID delete-user-from-event
// @Accept  json
// @Produce  json
// @Param input body common.DeleteUserFromPartyIn true "event info"
// @Success 204
// @Router /api/delete_user_from_party [post]
func (g *Gateway) DeleteUserFromParty(ctx *gin.Context) {
	var params common.DeleteUserFromPartyIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/delete_user_from_party")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserRoleOnParty
// @Security ApiKeyAuth
// @Tags event
// @Description Удаление пользователя с мероприятия
// @ID update-user-role
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserRoleOnPartyIn true "event info"
// @Success 204
// @Router /api/update_user_role_on_party [post]
func (g *Gateway) UpdateUserRoleOnParty(ctx *gin.Context) {
	var params common.UpdateUserRoleOnPartyIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_user_role_on_party")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary AddUserToParty
// @Security ApiKeyAuth
// @Tags event
// @Description Добавление пользователя на мероприятие
// @ID add-user-to-event
// @Accept  json
// @Produce  json
// @Param input body common.AddUserToPartyIn true "event info"
// @Success 204
// @Router /api/add_user_to_party [post]
func (g *Gateway) AddUserToParty(ctx *gin.Context) {
	var params common.AddUserToPartyIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	info, err := g.AuthClient.GetUsersInfo(ctx, common.GetUserInfoByIDIn{
		UserIDs: []int64{params.UserID},
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not get info. error: %v", err))
		return
	}

	if len(info) == 0 {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("no user with id: %d", params.UserID))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/add_user_to_party")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary AddListToEvent
// @Security ApiKeyAuth
// @Tags event
// @Description Добавление нового списка
// @ID create-list
// @Accept  json
// @Produce  json
// @Param input body common.AddListToEventIn true "event info"
// @Success 200 {object} common.AddListToEventOut
// @Router /api/add_list_to_event [post]
func (g *Gateway) AddListToEvent(ctx *gin.Context) {
	var params common.AddListToEventIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/add_list_to_event")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateListVisibility
// @Security ApiKeyAuth
// @Tags event
// @Description Виден ли список гостям
// @ID list-visibility
// @Accept  json
// @Produce  json
// @Param input body common.UpdateListVisibilityIn true "event info"
// @Success 204
// @Router /api/is_list_visible_to_guest [post]
func (g *Gateway) UpdateListVisibility(ctx *gin.Context) {
	var params common.UpdateListVisibilityIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/is_list_visible_to_guest")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary AddTaskToList
// @Security ApiKeyAuth
// @Tags event
// @Description Создание тасок в списке
// @ID create-tasks
// @Accept  json
// @Produce  json
// @Param input body common.AddTasksToListIn true "event info"
// @Success 204
// @Router /api/add_task_to_list [post]
func (g *Gateway) AddTaskToList(ctx *gin.Context) {
	var params common.AddTasksToListIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/add_task_to_list")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateTaskStatus
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление статуса таски (done/not done)
// @ID update-task-status
// @Accept  json
// @Produce  json
// @Param input body common.UpdateTaskStatusIn true "event info"
// @Success 204
// @Router /api/update_task_status [post]
func (g *Gateway) UpdateTaskStatus(ctx *gin.Context) {
	var params common.UpdateTaskStatusIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_task_status")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateTaskName
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление названия таски
// @ID update-task-name
// @Accept  json
// @Produce  json
// @Param input body common.UpdateTaskNameIn true "event info"
// @Success 204
// @Router /api/update_task_name [post]
func (g *Gateway) UpdateTaskName(ctx *gin.Context) {
	var params common.UpdateTaskNameIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_task_name")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetListInfo
// @Security ApiKeyAuth
// @Tags event
// @Description Получение инфы по списку
// @ID get-list-info
// @Accept  json
// @Produce  json
// @Param input body common.GetListInfoIn true "event info"
// @Success 200 {object} common.GetListInfoOut
// @Router /api/get_list_info [post]
func (g *Gateway) GetListInfo(ctx *gin.Context) {
	var params common.GetListInfoIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_list_info")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetPartyLists
// @Security ApiKeyAuth
// @Tags event
// @Description Получение списков мероприятия
// @ID get-party-lists
// @Accept  json
// @Produce  json
// @Param input body common.GetPartyListsIn true "event info"
// @Success 200 {object} common.GetPartyListsOut
// @Router /api/get_party_lists [post]
func (g *Gateway) GetPartyLists(ctx *gin.Context) {
	var params common.GetPartyListsIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_party_lists")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary DeleteList
// @Security ApiKeyAuth
// @Tags event
// @Description Удаление списка
// @ID delete-list
// @Accept  json
// @Produce  json
// @Param input body common.DeleteListIn true "event info"
// @Success 204
// @Router /api/delete_list [post]
func (g *Gateway) DeleteList(ctx *gin.Context) {
	var params common.DeleteListIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/delete_list")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateListType
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление типа списка (EMPTY TODO BUY WISHLIST)
// @ID update-list-type
// @Accept  json
// @Produce  json
// @Param input body common.UpdateListTypeIn true "event info"
// @Success 204
// @Router /api/update_list_type [post]
func (g *Gateway) UpdateListType(ctx *gin.Context) {
	var params common.UpdateListTypeIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_list_type")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetInviteLinkToEvent
// @Security ApiKeyAuth
// @Tags event
// @Description Получение диплинка на мероприятия (пригласительная ссылка)
// @ID get-event-invite-link
// @Accept  json
// @Produce  json
// @Param input body common.GetInviteLinkIn true "event info"
// @Success 200 {object} common.GetInviteLinkOut
// @Router /api/get_invite_link [post]
func (g *Gateway) GetInviteLinkToEvent(ctx *gin.Context) {
	var params common.GetInviteLinkIn
	if err := ctx.BindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(params)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_invite_link")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

func (g *Gateway) UploadReceipt(ctx *gin.Context) {
	userID := ctx.PostForm("userID")
	if userID == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid userID in")
		return
	}

	partyID := ctx.PostForm("partyID")
	if partyID == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid partyID in")
		return
	}

	name := ctx.PostForm("name")
	if name == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid name in")
		return
	}

	sum := ctx.PostForm("sum")
	if sum == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid sum	in")
		return
	}

	typee := ctx.PostForm("type")
	if typee == "" || (typee != "DEBT" && typee != "SUM") {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid type in")
		return
	}

	userIDint, err := strconv.ParseInt(userID, 10, 64)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid type userID in")
		return
	}
	partyIDint, err := strconv.ParseInt(partyID, 10, 64)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid type partyID in")
		return
	}
	sumFloat, err := strconv.ParseFloat(sum, 64)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "invalid type sum in")
		return
	}

	file, err := ctx.FormFile("file")
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, "could not get file")
		return
	}

	jsonResp, code, err := g.EventClient.UploadFile(ctx, name, typee, sumFloat, userIDint, partyIDint, "/update_receipt")
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, fmt.Sprintf("could not get resp from event service. error: %v", err))
		return
	}

	if code != http.StatusOK {
		ctx.String(code, string(jsonResp))
		return
	}

	var res common.UploadReceiptOut
	if err := jsoniter.Unmarshal(jsonResp, &res); err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, fmt.Sprintf("could not get unmarshal resp from event service. error: %v", err))
		return
	}

	err = ctx.SaveUploadedFile(file, fmt.Sprintf("receipt/partyID_%s/userID_%s/%d.pdf", partyID, userID, res.ExpensesID))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("error saving file: %v", err))
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// @Summary GetUserReceipt
// @Security ApiKeyAuth
// @Tags event
// @Description Получение чека пользователя по id чека
// @ID get-user-receipt
// @Accept  json
// @Produce  json
// @Param input body common.GetUserReceiptIn true "event info"
// @Router /api/get_user_receipt [post]
func (g *Gateway) GetUserReceipt(ctx *gin.Context) {
	var param common.GetUserReceiptIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	ctx.File(fmt.Sprintf("receipt/partyID_%d/userID_%d/%d.pdf", param.PartyID, param.UserID, param.ExpenseID))
}

// @Summary LoadFinanceState
// @Security ApiKeyAuth
// @Tags event
// @Description Получение стадии трат(?) мероприятия (параметр is_expenses)
// @ID load-finance-state
// @Accept  json
// @Produce  json
// @Param input body common.LoadFinanceStateIn true "event info"
// @Success 200 {object} common.LoadFinanceStateOut
// @Router /api/load_finance_state [post]
func (g *Gateway) LoadFinanceState(ctx *gin.Context) {
	var param common.LoadFinanceStateIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/load_finance_state")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateFinanceState
// @Security ApiKeyAuth
// @Tags event
// @Description Получение стадии трат(?) мероприятия (параметр is_expenses)
// @ID update-finance-state
// @Accept  json
// @Produce  json
// @Param input body common.UpdateFinanceStateIn true "event info"
// @Success 204
// @Router /api/update_finance_state [post]
func (g *Gateway) UpdateFinanceState(ctx *gin.Context) {
	var param common.UpdateFinanceStateIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_finance_state")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetUserExpenseInfoByID
// @Security ApiKeyAuth
// @Tags auth
// @Description Получение трат пользователя на айди расходов
// @ID get-user-expenses-by-id
// @Accept  json
// @Produce  json
// @Param input body common.GetUserExpenseInfoByIDIn true "event info"
// @Success 200 {object} common.GetUserExpenseInfoByIDOut
// @Router /api/get_user_expenses_by_id [post]
func (g *Gateway) GetUserExpenseInfoByID(ctx *gin.Context) {
	var param common.GetUserExpenseInfoByIDIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_user_expenses_by_id")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetPartySummaryExpenses
// @Security ApiKeyAuth
// @Tags auth
// @Description Получение суммарных трат на мероприятие
// @ID get-party-summary-expenses
// @Accept  json
// @Produce  json
// @Param input body common.GetPartySummaryExpensesIn true "event info"
// @Success 200 {object} common.GetPartySummaryExpensesOut
// @Router /api/get_party_summary_expenses [post]
func (g *Gateway) GetPartySummaryExpenses(ctx *gin.Context) {
	var param common.GetPartySummaryExpensesIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_party_summary_expenses")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetPartyWallet
// @Security ApiKeyAuth
// @Tags auth
// @Description Получение информации о кошельке мероприятия
// @ID get-party-wallet
// @Accept  json
// @Produce  json
// @Param input body common.GetPartyWalletIn true "event info"
// @Success 200 {object} common.GetPartyWalletOut
// @Router /api/get_party_wallet [post]
func (g *Gateway) GetPartyWallet(ctx *gin.Context) {
	var param common.GetPartyWalletIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_party_wallet")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetAllUserReceipt
// @Security ApiKeyAuth
// @Tags event
// @Description Получение всех чеков пользователя в одном пдф
// @ID get-all-user-receipt
// @Accept  json
// @Produce  json
// @Param input body common.GetAllUserReceiptIn true "event info"
// @Router /api/get_all_user_receipts [post]
func (g *Gateway) GetAllUserReceipt(ctx *gin.Context) {
	var param common.GetAllUserReceiptIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_all_user_receipts")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	if code == http.StatusNoContent {
		ctx.JSON(http.StatusNoContent, nil)
		return
	}

	if code != http.StatusOK {
		ctx.JSON(code, data)
		return
	}

	var info []common.UserExpenses
	err = jsoniter.Unmarshal(data, &info)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not unmarshal data from event service. error: %v", err))
		return
	}

	readers := make([]*model.PdfReader, 0, len(info))
	for i := range info {
		reader, _, err := model.NewPdfReaderFromFile(fmt.Sprintf("receipt/partyID_%d/userID_%d/%d.pdf", param.PartyID, info[0].UserID, info[i].Expenses), nil)
		if err != nil {
			continue
		}

		readers = append(readers, reader)
	}

	writer := model.NewPdfWriter()
	for _, reader := range readers {
		numPages, err := reader.GetNumPages()
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not read pages. error: %s", err))
			return
		}

		for i := 1; i <= numPages; i++ {
			page, err := reader.GetPage(i)
			if err != nil {
				ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("Error getting page %d: %v", i, err))
				//log.Fatalf("Error getting page %d: %v", i, err)
				return
			}

			err = writer.AddPage(page)
			if err != nil {
				ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("Error adding page %d: %v", i, err))
				//log.Fatalf("Error adding page %d: %v", i, err)
				return
			}

		}
	}

	err = writer.WriteToFile(fmt.Sprintf("receipt/partyID_%d/userID_%d/merge.pdf", param.PartyID, param.UserID))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("Error writing merged file: %v", err))
		logrus.Fatalf("Error writing merged file: %v", err)
		return
	}

	defer func() {
		_ = os.Remove(fmt.Sprintf("receipt/partyID_%d/userID_%d/merge.pdf", param.PartyID, param.UserID))
	}()
	ctx.File(fmt.Sprintf("receipt/partyID_%d/userID_%d/merge.pdf", param.PartyID, param.UserID))
}

// @Summary UpdateUserLogin
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление логина пользователя
// @ID update-user-login
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserLoginIn true "event info"
// @Success 204
// @Router /api/update_user_avatar [post]
func (g *Gateway) UpdateUserLogin(ctx *gin.Context) {
	var param common.UpdateUserLoginIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_login")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserAvatar
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление аватарки пользователя
// @ID update-user-avatar
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserAvatarIn true "event info"
// @Success 204
// @Router /api/update_user_avatar [post]
func (g *Gateway) UpdateUserAvatar(ctx *gin.Context) {
	var param common.UpdateUserAvatarIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_avatar")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserPassword
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление пароля пользователя
// @ID get-user-pass
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserPasswordIn true "event info"
// @Success 204
// @Router /api/update_user_password [post]
func (g *Gateway) UpdateUserPassword(ctx *gin.Context) {
	var param common.UpdateUserPasswordIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_password")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetUserWallet
// @Security ApiKeyAuth
// @Tags auth
// @Description Получение информации о кошельке пользователя
// @ID get-user-wallet
// @Accept  json
// @Produce  json
// @Param input body common.GetUserWalletIn true "event info"
// @Success 200 {object} common.GetUserWalletOut
// @Router /api/get_user_wallet [post]
func (g *Gateway) GetUserWallet(ctx *gin.Context) {
	var param common.GetUserWalletIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/get_user_wallet")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserWalletCard
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление карты кошелька пользователя
// @ID update-user-wallet-card
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserWalletCardIn true "event info"
// @Success 204
// @Router /api/update_user_wallet_card [post]
func (g *Gateway) UpdateUserWalletCard(ctx *gin.Context) {
	var param common.UpdateUserWalletCardIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_wallet_card")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserWalletOwner
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление владельца карты кошелька пользователя
// @ID update-user-wallet-owner
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserWalletOwnerIn true "event info"
// @Success 204
// @Router /api/update_user_wallet_owner [post]
func (g *Gateway) UpdateUserWalletOwner(ctx *gin.Context) {
	var param common.UpdateUserWalletOwnerIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_wallet_owner")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserWalletPhone
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление телефона кошелька пользователя
// @ID update-user-wallet-phone
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserWalletPhoneIn true "event info"
// @Success 204
// @Router /api/update_user_wallet_phone [post]
func (g *Gateway) UpdateUserWalletPhone(ctx *gin.Context) {
	var param common.UpdateUserWalletPhoneIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_wallet_phone")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdateUserWalletBank
// @Security ApiKeyAuth
// @Tags auth
// @Description Обновление банка пользователя
// @ID update-user-wallet-bank
// @Accept  json
// @Produce  json
// @Param input body common.UpdateUserWalletBankIn true "event info"
// @Success 204
// @Router /api/update_user_wallet_bank [post]
func (g *Gateway) UpdateUserWalletBank(ctx *gin.Context) {
	var param common.UpdateUserWalletBankIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.AuthClient.UpdateUserFields(ctx, jsonData, "/update_user_wallet_bank")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, data)
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetAllUserExpenses
// @Security ApiKeyAuth
// @Tags event
// @Description Получение информации по расходам пользователя (без чека, просто из базы) (на выходе массив моделек)
// @ID get-all-user-expenses
// @Accept  json
// @Produce  json
// @Param input body common.GetAllUserReceiptIn true "event info"
// @Success 200 {object} common.UserExpenses
// @Router /api/get_all_user_expenses [post]
func (g *Gateway) GetAllUserExpenses(ctx *gin.Context) {
	var param common.GetAllUserReceiptIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_all_user_receipts")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary CalculateExpenses
// @Security ApiKeyAuth
// @Tags event
// @Description Расчет сколько с человека надо собрать, сколько должен конкретный пользователь
// @ID calculate expenses
// @Accept  json
// @Produce  json
// @Param input body common.CalculateExpensesIn true "event info"
// @Success 200 {object} common.CalculateExpensesOut
// @Router /api/calculate_expenses [post]
func (g *Gateway) CalculateExpenses(ctx *gin.Context) {
	var param common.CalculateExpensesIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/calculate_expenses")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary UploadReceiptInfoSum
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление суммы чека
// @ID update-receipt-sum
// @Accept  json
// @Produce  json
// @Param input body common.UploadReceiptInfoSumIn true "event info"
// @Success 204
// @Router /api/update_receipt_info_sum [post]
func (g *Gateway) UploadReceiptInfoSum(ctx *gin.Context) {
	var param common.UploadReceiptInfoSumIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_receipt_info_sum")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary UploadReceiptInfoName
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление названия чека
// @ID update-receipt-name
// @Accept  json
// @Produce  json
// @Param input body common.UploadReceiptInfoNameIn true "event info"
// @Success 204
// @Router /api/update_receipt_info_name [post]
func (g *Gateway) UploadReceiptInfoName(ctx *gin.Context) {
	var param common.UploadReceiptInfoNameIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_receipt_info_name")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary UpdatePartyExpensesDeadline
// @Security ApiKeyAuth
// @Tags event
// @Description Обновление дедлайна шага трат
// @ID update-party-exepenses-deadlinу
// @Accept  json
// @Produce  json
// @Param input body common.UpdatePartyExpensesDeadlineIn true "event info"
// @Success 204
// @Router /api/update_party_expenses_deadline [post]
func (g *Gateway) UpdatePartyExpensesDeadline(ctx *gin.Context) {
	var param common.UpdatePartyExpensesDeadlineIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/update_party_expenses_deadline")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary GetPartyExpensesDeadline
// @Security ApiKeyAuth
// @Tags event
// @Description Получение текущего дедлайна шага трат
// @ID get-expenses-deadline
// @Accept  json
// @Produce  json
// @Param input body common.GetPartyExpensesDeadlineIn true "event info"
// @Success 200 {object} common.GetPartyExpensesDeadlineOut
// @Router /api/get_party_expenses_deadline [post]
func (g *Gateway) GetPartyExpensesDeadline(ctx *gin.Context) {
	var param common.GetPartyExpensesDeadlineIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/get_party_expenses_deadline")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary DeleteTaskFromList
// @Security ApiKeyAuth
// @Tags event
// @Description Удаление таски
// @ID delete-task-from-list
// @Accept  json
// @Produce  json
// @Param input body common.DeleteTaskFromListIn true "event info"
// @Success 204
// @Router /api/delete_task_from_list [post]
func (g *Gateway) DeleteTaskFromList(ctx *gin.Context) {
	var param common.DeleteTaskFromListIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	jsonData, err := jsoniter.Marshal(param)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not marshal body. error: %v", err))
		return
	}

	data, code, err := g.EventClient.UpdateEventFields(ctx, jsonData, "/delete_task_from_list")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not do request to event. error: %v", err))
		return
	}

	ctx.String(code, string(data))
}

// @Summary SendNotification
// @Security ApiKeyAuth
// @Tags notification
// @Description Отправка уведомления пользователю
// @ID push-notification
// @Accept  json
// @Produce  json
// @Param input body common.SendNotificationIn true "notification info"
// @Success 204
// @Router /push/send_notification [post]
func (g *Gateway) SendNotification(ctx *gin.Context) {
	var param common.SendNotificationIn
	if err := ctx.BindJSON(&param); err != nil {
		ctx.JSON(http.StatusBadRequest, fmt.Sprintf("could not validate fields in body. error: %v", err))
		return
	}

	topic := strconv.Itoa(int(param.UserID))

	err := g.Firebase.SendMessage(ctx, topic, param.Title, param.Body)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, fmt.Sprintf("could not send notification. error: %v", err))
		return
	}

	ctx.JSON(http.StatusNoContent, nil)
}
