package service

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	"gitlab.com/fokurly/tamada-app/gateway/common"
)

func (g *Gateway) CheckAuth(ctx *gin.Context) {
	header := ctx.GetHeader(common.AuthorizationHeader)
	if header == "" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}
	tokenParts := strings.Split(header, " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	if len(tokenParts[1]) == 0 {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "empty auth header")
		return
	}

	code, err := g.AuthClient.CheckToken(ctx, header)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, fmt.Sprintf("could not check token in auth service. error: %v", err))
		return
	}

	if code != http.StatusNoContent {
		ctx.AbortWithStatusJSON(code, nil)
		return
	}

	ctx.Next()
}
