package service

import (
	"context"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/option"
)

type NotificationService struct {
	Cli *messaging.Client
}

func NewNotificationService(ctx context.Context) *NotificationService {
	opt := option.WithCredentialsFile("tamada-application-firebase.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		logrus.Panicf("error initializing app: %v", err)
	}

	client, err := app.Messaging(ctx)
	if err != nil {
		logrus.Panicf("error initialize. err: %v", err)
	}

	return &NotificationService{
		Cli: client,
	}
}

func (n *NotificationService) SendMessage(ctx context.Context, topic, title, body string) error {
	message := &messaging.Message{
		Notification: &messaging.Notification{
			Title: title,
			Body:  body,
		},
		Topic: topic,
	}
	response, err := n.Cli.Send(ctx, message)
	if err != nil {
		return err
	}
	// Response is a message ID string.
	logrus.Debugf("Successfully sent message: %s", response)
	return nil
}
