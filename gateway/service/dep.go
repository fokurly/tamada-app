package service

import (
	"context"

	"gitlab.com/fokurly/tamada-app/gateway/common"
)

type Gateway struct {
	EventClient EventClient
	AuthClient  AuthClient
	UserClient  UserClient
	Firebase    Notification
}

func NewGatewayService(event EventClient, auth AuthClient, user UserClient, notification Notification) *Gateway {
	return &Gateway{
		EventClient: event,
		UserClient:  user,
		AuthClient:  auth,
		Firebase:    notification,
	}
}

type EventClient interface {
	CreateEvent(ctx context.Context, in common.CreateEventIn) ([]byte, int, error)
	GetEvent(ctx context.Context, in common.GetEventIn) ([]byte, int, error)
	GetUserEvents(ctx context.Context, in common.GetUserEventsIn) ([]byte, int, error)
	GetPartiesGuests(ctx context.Context, in common.GetPartiesGuestsIn) ([]common.GetPartiesGuestsClientResp, error)
	UpdateEventCoverID(ctx context.Context, in common.UpdateEventCoverIDIn) ([]byte, int, error)
	UpdateEventFields(ctx context.Context, jsonData []byte, endpoint string) ([]byte, int, error)
	UploadFile(ctx context.Context, name, typee string, sum float64, userID, partyID int64, endpoint string) ([]byte, int, error)
}

type AuthClient interface {
	Ping(ctx context.Context) ([]byte, int, error)
	Login(ctx context.Context, in common.LoginIn) ([]byte, int, error)
	Register(ctx context.Context, in common.RegisterIn) ([]byte, int, error)
	RefreshToken(ctx context.Context, token string) ([]byte, int, error)
	CheckToken(ctx context.Context, token string) (int, error)
	GetUsersInfo(ctx context.Context, in common.GetUserInfoByIDIn) ([]common.GetUserInfoByIDOut, error)
	DeleteUser(ctx context.Context, in common.DeleteUserIn) (int, error)
	UpdateUserFields(ctx context.Context, jsonData []byte, endpoint string) ([]byte, int, error)
}
type UserClient interface {
}

type Notification interface {
	SendMessage(ctx context.Context, topic, title, body string) error
}
