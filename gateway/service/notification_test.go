package service

import (
	"context"
	"fmt"
	"log"
	"testing"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/option"
)

func Test(t *testing.T) {
	opt := option.WithCredentialsFile("../tamada-application-firebase-adminsdk-vl4ux-d7ec85f8ad.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		logrus.Panicf("error initializing app: %v", err)
	}

	client, err := app.Messaging(context.Background())
	if err != nil {
		logrus.Panicf("error initialize. err: %v", err)
	}

	message := &messaging.Message{
		Notification: &messaging.Notification{
			Title: "тестовый тест",
			Body:  "Съешь ещё этих мягких французских булок, да выпей же чаю",
		},
		//Data: map[string]string{
		//	"приветули": "850",
		//	"time":      "2:45",
		//},
		Topic: "test_Artem",
	}
	response, err := client.Send(context.Background(), message)
	if err != nil {
		log.Fatalln(err)
	}
	// Response is a message ID string.
	fmt.Println("Successfully sent message:", response)
}
