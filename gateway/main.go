package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/unidoc/unipdf/v3/common/license"
	"golang.org/x/sync/errgroup"

	"gitlab.com/fokurly/tamada-app/gateway/clients"
	"gitlab.com/fokurly/tamada-app/gateway/service"
)

// @title Tamada-app Gateway
// @version 1.0
// @description Gateway обработчик

// @host 158.160.59.43:8080/
// @BasePath

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	mainCtx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()
	authUrl := os.Getenv("AUTH_SERVICE_ADDRESS")
	eventUrl := os.Getenv("EVENT_SERVICE_ADDRESS")
	userUrl := os.Getenv("USER_SERVICE_ADDRESS")
	if authUrl == "" || eventUrl == "" || userUrl == "" {
		authUrl = "http://localhost:8081"
		eventUrl = "http://localhost:8082"
		userUrl = "http://localhost:8083"
	}
	authCli := clients.NewAuthClient(authUrl)
	eventCli := clients.NewEventClient(eventUrl)
	userCli := clients.NewEventClient(userUrl)
	firebaseClie := service.NewNotificationService(mainCtx)
	gateway := service.NewGatewayService(eventCli, authCli, userCli, firebaseClie)
	router := gin.Default()

	router.GET("/ping", gateway.Ping)
	router.POST("/login", gateway.Login)
	router.POST("/register", gateway.Register)

	authGroup := router.Group("/api", gateway.CheckAuth)
	{
		authGroup.GET("/refresh_token", gateway.RefreshToken)
		authGroup.POST("/delete_user", gateway.DeleteUser)
		authGroup.POST("/update_user_login", gateway.UpdateUserLogin)
		authGroup.POST("/update_user_avatar", gateway.UpdateUserAvatar)
		authGroup.POST("/update_user_password", gateway.UpdateUserPassword)
		//
		authGroup.POST("/get_user_wallet", gateway.GetUserWallet)
		//
		authGroup.POST("/update_user_wallet_card", gateway.UpdateUserWalletCard)
		authGroup.POST("/update_user_wallet_owner", gateway.UpdateUserWalletOwner)
		authGroup.POST("/update_user_wallet_phone", gateway.UpdateUserWalletPhone)
		authGroup.POST("/update_user_wallet_bank", gateway.UpdateUserWalletBank)
	}

	eventGroup := router.Group("/api", gateway.CheckAuth)
	{
		eventGroup.POST("/create_event", gateway.CreateEvent)
		eventGroup.POST("/get_event", gateway.GetEvent)

		// перенести в юзера?
		eventGroup.POST("/get_user_parties", gateway.GetUserEvents)
		eventGroup.POST("/get_parties_guests", gateway.GetPartiesGuests)
		eventGroup.POST("/delete_task_from_list", gateway.DeleteTaskFromList)
		eventGroup.POST("/update_event_cover_id", gateway.UpdateEventCoverID)
		eventGroup.POST("/update_event_moodboard_link", gateway.UpdateEventMoodboardLink)
		eventGroup.POST("/update_event_dress_code", gateway.UpdateEventDressCode)
		eventGroup.POST("/update_event_theme", gateway.UpdateEventTheme)
		eventGroup.POST("/update_event_important", gateway.UpdateEventImportant)
		eventGroup.POST("/update_event_is_expenses", gateway.UpdateEventIsExpenses)
		eventGroup.POST("/update_event_address_additional", gateway.UpdateEventAddressAdditionalInfo)
		eventGroup.POST("/update_event_address_link", gateway.UpdateEventAddressLink)
		eventGroup.POST("/update_event_address", gateway.UpdateEventAddress)
		eventGroup.POST("/update_event_end_time", gateway.UpdateEventEndTime)
		eventGroup.POST("/update_event_start_time", gateway.UpdateEventStartTime)

		eventGroup.POST("/update_event_name", gateway.UpdateEventName)

		eventGroup.POST("/delete_user_from_party", gateway.DeleteUserFromParty)
		eventGroup.POST("/update_user_role_on_party", gateway.UpdateUserRoleOnParty)
		eventGroup.POST("/add_user_to_party", gateway.AddUserToParty)

		eventGroup.POST("/add_list_to_event", gateway.AddListToEvent)
		eventGroup.POST("/is_list_visible_to_guest", gateway.UpdateListVisibility)
		eventGroup.POST("/add_task_to_list", gateway.AddTaskToList)
		eventGroup.POST("/update_task_status", gateway.UpdateTaskStatus)
		eventGroup.POST("/update_task_name", gateway.UpdateTaskName)
		eventGroup.POST("/get_list_info", gateway.GetListInfo)
		//
		eventGroup.POST("/get_party_lists", gateway.GetPartyLists)
		eventGroup.POST("/delete_list", gateway.DeleteList)
		eventGroup.POST("/update_list_type", gateway.UpdateListType)

		eventGroup.POST("/get_invite_link", gateway.GetInviteLinkToEvent)
		eventGroup.POST("/load_finance_state", gateway.LoadFinanceState)
		eventGroup.POST("/update_finance_state", gateway.UpdateFinanceState)
		eventGroup.POST("/update_receipt", gateway.UploadReceipt)
		eventGroup.POST("/get_all_user_receipts", gateway.GetAllUserReceipt)
		eventGroup.POST("/get_all_user_expenses", gateway.GetAllUserExpenses)
		eventGroup.POST("/get_user_receipt", gateway.GetUserReceipt)

		eventGroup.POST("/get_user_expenses_by_id", gateway.GetUserExpenseInfoByID)
		eventGroup.POST("/get_party_summary_expenses", gateway.GetPartySummaryExpenses)

		eventGroup.POST("/get_party_wallet", gateway.GetPartyWallet)

		eventGroup.POST("/update_party_wallet_owner", gateway.UpdatePartyWalletOwner)
		eventGroup.POST("/update_party_wallet_bank", gateway.UpdatePartyWalletBank)
		eventGroup.POST("/update_party_wallet_phone", gateway.UpdatePartyWalletPhone)
		eventGroup.POST("/update_party_wallet_card", gateway.UpdatePartyWalletCard)

		eventGroup.POST("/calculate_expenses", gateway.CalculateExpenses)

		eventGroup.POST("/update_receipt_info_sum", gateway.UploadReceiptInfoSum)
		eventGroup.POST("/update_receipt_info_name", gateway.UploadReceiptInfoName)
		eventGroup.POST("/update_party_expenses_deadline", gateway.UpdatePartyExpensesDeadline)
		eventGroup.POST("/get_party_expenses_deadline", gateway.GetPartyExpensesDeadline)
	}

	firebase := router.Group("/push", gateway.CheckAuth)
	{
		firebase.POST("/send_notification", gateway.SendNotification)
	}

	server := http.Server{
		Addr:         ":8080",
		Handler:      router,
		WriteTimeout: 25 * time.Second,
		ReadTimeout:  25 * time.Second,
	}
	g, gCtx := errgroup.WithContext(mainCtx)
	g.Go(func() error {
		return server.ListenAndServe()
	})
	g.Go(func() error {
		<-gCtx.Done()
		return server.Shutdown(context.Background())
	})

	if err := g.Wait(); err != nil {
		logrus.Print("exit reason: %s \n", err)
	}
}

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		PrettyPrint:     true,
	})

	err := license.SetMeteredKey("d0979a2d9f82cc56c5f9681964ec5405de3cdbc6a52a6df02b0bf12e62de864d")
	if err != nil {
		panic("err license")
	}
}
